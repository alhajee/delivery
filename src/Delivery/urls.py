from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt  # New library
from graphql_jwt.decorators import jwt_cookie
from graphene_django.views import GraphQLView
from django.contrib.staticfiles.storage import staticfiles_storage
from django.views.generic.base import RedirectView

from .schema import schema


urlpatterns = [
    path('favicon.ico', RedirectView.as_view(url=staticfiles_storage.url('img/favicon.ico'))),
    path('admin/', admin.site.urls),
    path('graphql/', csrf_exempt(jwt_cookie(GraphQLView.as_view(graphiql=True, schema=schema)))),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
