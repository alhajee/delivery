from django.contrib import admin

from .models import Courier, Driver, DeliveryProfile
from contacts.models import Address, Contact


class CourierAdmin(admin.ModelAdmin):
    list_display = ('name', 'get_contact', 'get_state',)
    list_filter = ('address__state', )
    search_fields = ['name', 'contact__phone', 'contact__phone']

    def get_contact(self, obj):
        return f"{obj.contact.name} ({obj.contact.phone})"

    def get_state(self, obj):
        return obj.address.state

    get_contact.short_description = 'Contact'
    get_state.short_description = 'State'
    get_state.admin_order_field = 'address__state'


class DeliveryProfileAdmin(admin.ModelAdmin):
    list_display = ('title', 'get_courier', 'origin', 'destination',
                    'scope', 'price', 'active')
    list_filter = ('active', 'scope', 'origin', 'destination')
    search_fields = ['title', ]

    def get_courier(self, obj):
        return obj.courier.name

    get_courier.short_description = 'Courier'


class DriverAdmin(admin.ModelAdmin):

    list_display = ('get_fullname', 'get_phone', 'get_courier')
    list_filter = ('courier',)
    search_fields = ['get_fullname', 'get_phone', ]

    def get_fullname(self, obj):
        return f"{obj.user.first_name} {obj.user.last_name}"

    def get_phone(self, obj):
        return obj.user.phone

    def get_courier(self, obj):
        return obj.courier.name

    get_fullname.short_description = 'Full Name'
    get_phone.short_description = 'Phone'
    get_courier.short_description = 'Courier'


admin.site.register(Courier, CourierAdmin)
admin.site.register(Driver, DriverAdmin)
admin.site.register(DeliveryProfile, DeliveryProfileAdmin)
