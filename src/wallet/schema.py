# Django libraries
from django.db.models import Q
from django.db import IntegrityError

import graphene
from graphql import GraphQLError
from graphene_django.types import DjangoObjectType
from graphql_jwt.decorators import login_required
from graphql_jwt.decorators import user_passes_test
from graphql_jwt.decorators import permission_required
from graphql_jwt.decorators import staff_member_required
from graphql_jwt.decorators import superuser_required

# Project libraries
from .models import Wallet
from .models import create_wallet as create_wallet_request
from accounts.models import User


class WalletType(DjangoObjectType):
  class Meta:
    model = Wallet


class Query(object):
  all_wallets = graphene.List(WalletType, user=graphene.ID())
  wallet = graphene.Field(WalletType, id=graphene.ID())

  @superuser_required
  def resolve_all_wallets(self, info, user=None, **kwargs):
    if user:
      # Query wallet by user id
      return Wallet.objects.filter(user=user)
    else:
      # Querying all wallets
      return Wallet.objects.all()

  @login_required
  def resolve_wallet(self, info, id):
    # Querying a single business
    return Wallet.objects.get(pk=id)


########################BUSINESS##########################
class CreateWallet(graphene.Mutation):
  wallet = graphene.Field(WalletType)

  class Arguments:
    # The input arguments for this mutation
    user = graphene.ID()
    currency_code = graphene.String()
    bvn = graphene.String()
  
  @login_required
  def mutate(self, info, user, currency_code=None, bvn=None):
    response = None # JSON Response from Remote Endpoint
    
    currency_code = "NGN" if currency_code is None else currency_code

    # try to fetch user details
    try:
      user = User.objects.get(pk=user)
    except User.DoesNotExist:
      raise GraphQLError({
        'ok': False,
        'message': 'User does not exists',
        'error_code': 'USER_NOT_FOUND'
        })

    # Create a new wallet
    try:
      response = create_wallet_request(
        user=user,
        currency_code=currency_code,
        account_bvn=bvn
      )
    except Exception as e:
      #TODO: Change error message to [e.message]
      raise GraphQLError({
        'ok': False,
        'message': str(e),
        'error_code': 'FAILED_TO_CREATE_WALLET'
        })

    # Save wallet details to Database
    if response:
      #TODO: Error handling for each key in response
      account_number = response.get("accountNumber")
      bank_name = response.get("bankName")
      bank_code = response.get("bankCode")
      currency_code = response.get("currencyCode")
      reference = response.get("reservationReference")
      account_type = response.get("reservedAccountType")
      status = response.get("status")

      print(account_type)
      try:
        wallet = Wallet.objects.create(
          user=user,
          account_number=account_number,
          bank_name=bank_name,
          bank_code=bank_code,
          currency_code=currency_code,
          reference=reference,
          account_type=account_type,
          status=status
        )
      except IntegrityError as e:
        raise GraphQLError({
          'ok': False,
          'message': 'Wallet with the same currency code already exist',
          'error_code': 'WALLET_ALREADY_EXIST'
          })

    return CreateWallet(
      wallet=wallet
    )


class UpdateWallet(graphene.Mutation):
  # The class attributes define the response of the mutation
  wallet = graphene.Field(WalletType)

  class Arguments:
    # The input arguments for this mutation
    id = graphene.ID()
    account_number = graphene.String()
    bank_name = graphene.String()
    bank_code = graphene.String()
    currency_code = graphene.String()
    reference = graphene.String()
    account_type = graphene.String()

  @superuser_required
  def mutate(self, info, id, account_number=None, bank_name=None, bank_code=None, currency_code=None, reference=None, account_type=None):
    try:
      wallet = Wallet.objects.get(pk=id)
      wallet.account_number = account_number if account_number is not None else wallet.account_number
      wallet.bank_name = bank_name if bank_name is not None else wallet.bank_name
      wallet.bank_code = bank_code if bank_code is not None else wallet.bank_code
      wallet.currency_code = currency_code if currency_code is not None else wallet.currency_code
      wallet.reference = reference if reference is not None else wallet.reference
      wallet.account_type = account_type if account_type is not None else wallet.account_type
      wallet.save()
    except Wallet.DoesNotExist:
      raise GraphQLError({
        'ok': False,
        'message': 'Wallet does not exists',
        'error_code': 'WALLET_NOT_FOUND'
        })

    # Notice we return an instance of this mutation
    return UpdateWallet(wallet=wallet)

class DeleteWallet(graphene.Mutation):
  #TODO: Change response to boolean
  class Arguments:
    # The input arguments for this mutation
    id = graphene.ID()

  # The class attributes define the response of the mutation
  wallet = graphene.Field(WalletType)
  ok = graphene.Boolean()

  @login_required
  def mutate(self, info, id):
    try:
      wallet = Wallet.objects.get(pk=id)
    except Wallet.DoesNotExist:
      raise GraphQLError({
        'ok': False,
        'message': 'Wallet does not exists',
        'error_code': 'WALLET_NOT_FOUND'
        })

    # Notice we return an instance of this mutation
    return DeleteWallet(wallet=wallet, ok=True)


class Mutation(graphene.ObjectType):
  create_wallet = CreateWallet.Field()
  update_wallet = UpdateWallet.Field()
  delete_wallet = DeleteWallet.Field()