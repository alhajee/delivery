import graphene
from graphene import Argument
from graphene_django.types import DjangoObjectType

from graphql_jwt.decorators import login_required
from graphql_jwt.decorators import user_passes_test
from graphql_jwt.decorators import permission_required
from graphql_jwt.decorators import staff_member_required
from graphql_jwt.decorators import superuser_required
from graphql import GraphQLError
from django.db.models import Q

from .models import Media, Event, Order
from accounts.models import User
from business.models import Business
from couriers.models import Courier
from contacts.models import Address, Contact

from contacts.schema import AddressInputType
from contacts.schema import ContactInputType
from contacts.schema import CreateAddress
from contacts.schema import CreateContact


class MediaType(DjangoObjectType):
  class Meta:
    model = Media

class EventType(DjangoObjectType):
  class Meta:
    model = Event

class OrderType(DjangoObjectType):
  class Meta:
    model = Order


class Query(object):
  all_media = graphene.List(MediaType)
  media = graphene.Field(MediaType, id=graphene.ID())

  all_events = graphene.List(EventType)
  event = graphene.Field(EventType, id=graphene.ID())

  all_orders = graphene.List(OrderType)
  order = graphene.Field(OrderType, id=graphene.ID())

  @login_required
  def resolve_all_events(self, info, **kwargs):
    # Querying a list of events
    return Event.objects.all()

  @login_required
  def resolve_event(self, info, id):
    # Querying a single event
    return Event.objects.get(pk=id)

  @login_required
  def resolve_all_orders(self, info, **kwargs):
    # Querying a list of orders
    return Order.objects.all()

  @login_required
  def resolve_order(self, info, id):
    # Querying a single order
    return Order.objects.get(pk=id)

########################UPLOAD##########################
class Upload(graphene.Scalar):
    def serialize(self):
        pass

########################MEDIA##########################
class CreateMedia(graphene.Mutation):
  class Arguments:
    description = graphene.String()
    document = Upload()
    uploaded_at = graphene.types.datetime.DateTime()

  media = graphene.Field(MediaType)

  @login_required
  def mutate(self, info, description=None, uploaded_at=None):
    
    if info.context.FILES and info.context.method == 'POST':
      document = info.context.FILES['document']
      media = Media.objects.create(
        description = description,
        uploaded_at = uploaded_at,
        document = document
      )

    return CreateMedia(
      media=media
    )

class UpdateMedia(graphene.Mutation):
  class Arguments:
    # The input arguments for this mutation
    id = graphene.ID()
    description = graphene.String()
    document = Upload()
    uploaded_at = graphene.types.datetime.DateTime()

  # The class attributes define the response of the mutation
  media = graphene.Field(MediaType)

  @login_required
  def mutate(self, info, id, document=None, description=None, uploaded_at=None):
    
    if info.context.FILES and info.context.method == 'POST':
      document = info.context.FILES['document']
    
    try:
      media = Media.objects.get(pk=id)
      media.document = document if document is not None else media.document
      media.description = description if description is not None else media.description
      media.uploaded_at = uploaded_at if uploaded_at is not None else media.uploaded_at
      media.save()
    except Media.DoesNotExist:
      raise GraphQLError({
        'message': 'Media does not exists',
        'error_code': 'MEDIA_NOT_FOUND'
        })
    # Notice we return an instance of this mutation
    return UpdateMedia(media=media)

class DeleteMedia(graphene.Mutation):
  class Arguments:
    # The input arguments for this mutation
    id = graphene.ID()

  # The class attributes define the response of the mutation
  media = graphene.Field(MediaType)
  ok = graphene.Boolean()

  @login_required
  def mutate(self, info, id):
    try:
      media = Media.objects.get(pk=id)

      if media is not None:
        media.delete()
    except Media.DoesNotExist:
      raise GraphQLError({
        'message': 'Media does not exists',
        'error_code': 'MEDIA_NOT_FOUND'
        })

    # Notice we return an instance of this mutation
    return DeleteMedia(media=media, ok=True)

########################EVENT##########################
class CreateEvent(graphene.Mutation):
  class Arguments:
    # The input arguments for this mutation
    description = graphene.String()
    timestamp = graphene.types.datetime.DateTime()

  event = graphene.Field(EventType)

  @staff_member_required
  def mutate(self, info, description, timestamp=None):

    event = Event.objects.create(
      description = description,
      timestamp = timestamp
    )

    return CreateEvent(
      event=event
    )


class UpdateEvent(graphene.Mutation):
  class Arguments:
    # The input arguments for this mutation
    id = graphene.ID()
    description = graphene.String()
    timestamp = graphene.types.datetime.DateTime()

  # The class attributes define the response of the mutation
  event = graphene.Field(EventType)

  @staff_member_required
  def mutate(self, info, id, description=None, timestamp=None):
    try:
      event = Event.objects.get(pk=id)
      event.description = description if description is not None else event.description
      event.timestamp = timestamp if timestamp is not None else event.timestamp
      event.save()
    except Event.DoesNotExist:
      raise GraphQLError({
        'message': 'Event does not exists',
        'error_code': 'EVENT_NOT_FOUND'
        })
    # Notice we return an instance of this mutation
    return UpdateEvent(event=event)


class DeleteEvent(graphene.Mutation):
  class Arguments:
    # The input arguments for this mutation
    id = graphene.ID()

  # The class attributes define the response of the mutation
  event = graphene.Field(EventType)
  ok = graphene.Boolean()

  @staff_member_required
  def mutate(self, info, id):
    try:
      event = Event.objects.get(pk=id)

      if event is not None:
        event.delete()
    except Event.DoesNotExist:
      raise GraphQLError({
        'message': 'Event does not exists',
        'error_code': 'EVENT_NOT_FOUND'
        })
    # Notice we return an instance of this mutation
    return DeleteEvent(event=event, ok=True)

########################ORDER##########################
class CreateOrder(graphene.Mutation):
  class Arguments:
    # The input arguments for this mutation
    user = graphene.ID()
    tracking_id = graphene.String()
    status = graphene.String()
    business = graphene.ID()

    pickup_contact = graphene.Argument(ContactInputType)
    pickup_address = graphene.Argument(AddressInputType)

    delivery_contact = graphene.Argument(ContactInputType)
    delivery_address = graphene.Argument(AddressInputType)

    created_at = graphene.types.datetime.DateTime()
    updated_at = graphene.types.datetime.DateTime()
    description = graphene.String()
    media = graphene.List(graphene.ID)
    assigned_to = graphene.ID()
    quantity = graphene.Int()
    event = graphene.List(graphene.ID)
    dimension = graphene.String()
    weight = graphene.Float()
    note = graphene.String()
    charges = graphene.Float()

  order = graphene.Field(OrderType)

  @login_required
  def mutate(self, info, user, status, business, delivery_contact, delivery_address, pickup_contact=None, pickup_address=None, tracking_id=None, created_at=None, updated_at=None, description=None, media=None, assigned_to=None, quantity=None, event=None, dimension=None, weight=None, note=None, charges=None):
    try:
      user = User.objects.get(pk=user) if user is not None else user 
    except User.DoesNotExist:
      raise GraphQLError({
        'message': 'User does not exists',
        'error_code': 'USER_NOT_FOUND'
        })

    try:
      business = Business.objects.get(pk=business) if business is not None else business
    except Business.DoesNotExist:
      raise GraphQLError({
        'message': 'Business does not exists',
        'error_code': 'BUSINESS_NOT_FOUND'
        })

    try:
      if pickup_contact is not None:
        pickup_contact, created = Contact.objects.filter(Q(pk=pickup_contact.id)).get_or_create(
          name=pickup_contact.name,
          phone=pickup_contact.phone,
          address=pickup_contact.address,
          email=pickup_contact.email
        )
      # pickup_contact = Contact.objects.get(pk=pickup_contact) if pickup_contact is not None else pickup_contact
    except Contact.DoesNotExist:
      raise GraphQLError({
        'message': 'Pickup Contact does not exists',
        'error_code': 'CONTACT_NOT_FOUND'
        })

    try:
      if pickup_address is not None:
        pickup_address, created = Address.objects.filter(Q(pk=pickup_address.id)).get_or_create(
          country=pickup_address.country,
          state=pickup_address.state,
          city=pickup_address.city,
          zipcode=pickup_address.zipcode,
          address_line1=pickup_address.address_line1,
          address_line2=pickup_address.address_line2
        )
      # pickup_address = Address.objects.get(pk=pickup_address) if pickup_address is not None else pickup_address
    except Address.DoesNotExist:
      raise GraphQLError({
        'message': 'Pickup Address does not exists',
        'error_code': 'ADDRESS_NOT_FOUND'
        })

    try:
      if delivery_contact is not None:
        delivery_contact, created = Contact.objects.filter(Q(pk=delivery_contact.id)).get_or_create(
          name=delivery_contact.name,
          phone=delivery_contact.phone,
          address=delivery_contact.address,
          email=delivery_contact.email
        )
      # delivery_contact = Contact.objects.get(pk=delivery_contact) if delivery_contact is not None else delivery_contact
    except Contact.DoesNotExist:
      raise GraphQLError({
        'message': 'Delivery Contact does not exists',
        'error_code': 'CONTACT_NOT_FOUND'
        })

    try:
      delivery_address, created = Address.objects.filter(Q(pk=delivery_address.id)).get_or_create(
          country=delivery_address.country,
          state=delivery_address.state,
          city=delivery_address.city,
          zipcode=delivery_address.zipcode,
          address_line1=delivery_address.address_line1,
          address_line2=delivery_address.address_line2
      )
      # delivery_address = Address.objects.get(pk=delivery_address) if delivery_address is not None else delivery_address
    except Address.DoesNotExist:
      raise GraphQLError({
        'message': 'Delivery Address does not exists',
        'error_code': 'ADDRESS_NOT_FOUND'
        })

    try:
      assigned_to = Courier.objects.get(pk=assigned_to) if assigned_to is not None else assigned_to
    except Courier.DoesNotExist:
      raise GraphQLError({
        'message': 'Courier does not exists',
        'error_code': 'COURRIER_NOT_FOUND'
        })

    order = Order(
      user = user,
      status = status,
      business = business,
      pickup_contact = pickup_contact,
      pickup_address = pickup_address,
      delivery_contact = delivery_contact,
      delivery_address = delivery_address,
      description = description,
      assigned_to = assigned_to,
      quantity = quantity,
      dimension = dimension,
      weight = weight,
      note = note,
      charges = charges
    )

    if media is not None:
      media_set = []
      for media_id in media:
        try:
          media_instance = Media.objects.get(pk=media_id)
          media_set.append(media_instance)
        except Media.DoesNotExist:
          raise GraphQLError({
            'message': 'Media does not exists',
            'error_code': 'MEDIA_NOT_FOUND'
            })
      order.media.set(media_set)

    if event is not None:
      event_set = []
      for event_id in event:
        try:
          event_instance = Event.objects.get(pk=event_id)
          event_set.append(event_instance)
        except Event.DoesNotExist:
          raise GraphQLError({
            'message': 'Event does not exists',
            'error_code': 'EVENT_NOT_FOUND'
            })
      order.event.set(event_set)
    
    order.save()
    return CreateOrder(
      order=order
    )

class UpdateOrder(graphene.Mutation):
  class Arguments:
    # The input arguments for this mutation
    id = graphene.ID()
    user = graphene.ID()
    tracking_id = graphene.String()
    status = graphene.String()
    business = graphene.ID()

    pickup_contact = graphene.ID()
    pickup_address = graphene.ID()

    delivery_contact = graphene.ID()
    delivery_address = graphene.ID()

    created_at = graphene.types.datetime.DateTime()
    updated_at = graphene.types.datetime.DateTime()
    description = graphene.String()
    media = graphene.List(graphene.ID)
    assigned_to = graphene.ID()
    quantity = graphene.Int()
    event = graphene.List(graphene.ID)
    dimension = graphene.String()
    weight = graphene.Float()
    note = graphene.String()
    charges = graphene.Float()

  # The class attributes define the response of the mutation
  order = graphene.Field(OrderType)

  @login_required
  def mutate(self, info, id, user=None, status=None, business=None, pickup_contact=None, pickup_address=None, delivery_contact=None, delivery_address=None, tracking_id=None, created_at=None, updated_at=None, description=None, media=None, assigned_to=None, quantity=None, event=None, dimension=None, weight=None, note=None, charges=None):
    # First, Get foreign key objects
    try:
      user = User.objects.get(pk=user) if user is not None else user 
    except User.DoesNotExist:
      raise GraphQLError({
        'message': 'User does not exists',
        'error_code': 'USER_NOT_FOUND'
        })

    try:
      business = Business.objects.get(pk=business) if business is not None else business
    except Business.DoesNotExist:
      raise GraphQLError({
        'message': 'Business does not exists',
        'error_code': 'BUSINESS_NOT_FOUND'
        })

    try:
      pickup_contact = Contact.objects.get(pk=pickup_contact) if pickup_contact is not None else pickup_contact
    except Contact.DoesNotExist:
      raise GraphQLError({
        'message': 'Pickup Contact does not exists',
        'error_code': 'CONTACT_NOT_FOUND'
        })

    try:
      pickup_address = Address.objects.get(pk=pickup_address) if pickup_address is not None else pickup_address
    except Address.DoesNotExist:
      raise GraphQLError({
        'message': 'Pickup Address does not exists',
        'error_code': 'ADDRESS_NOT_FOUND'
        })

    try:
      delivery_contact = Contact.objects.get(pk=delivery_contact) if delivery_contact is not None else delivery_contact
    except Contact.DoesNotExist:
      raise GraphQLError({
        'message': 'Delivery Contact does not exists',
        'error_code': 'CONTACT_NOT_FOUND'
        })

    try:
      delivery_address = Address.objects.get(pk=delivery_address) if delivery_address is not None else delivery_address
    except Address.DoesNotExist:
      raise GraphQLError({
        'message': 'Delivery Address does not exists',
        'error_code': 'ADDRESS_NOT_FOUND'
        })

    try:
      assigned_to = Courier.objects.get(pk=assigned_to) if assigned_to is not None else assigned_to
    except Courier.DoesNotExist:
      raise GraphQLError({
        'message': 'Courier does not exists',
        'error_code': 'COURRIER_NOT_FOUND'
        })

    try:
      order = Order.objects.get(pk=id)
      order.user = user if user is not None else user
      order.tracking_id = tracking_id if tracking_id is not None else order.tracking_id
      order.status = status if status is not None else order.status
      order.business = business if business is not None else order.business
      order.pickup_contact = pickup_contact if pickup_contact is not None else pickup_contact
      order.pickup_address = pickup_address if pickup_address is not None else pickup_address
      order.delivery_contact = delivery_contact if delivery_contact is not None else delivery_contact
      order.delivery_address = delivery_address if delivery_address is not None else delivery_address
      order.created_at = created_at if created_at is not None else order.created_at
      order.updated_at = updated_at if updated_at is not None else order.updated_at
      order.description = description if description is not None else order.description
      order.media = media if media is not None else order.media
      order.assigned_to = assigned_to if assigned_to is not None else order.assigned_to
      order.quantity = quantity if quantity is not None else order.quantity
      order.event = event if event is not None else order.event
      order.dimension = dimension if dimension is not None else order.dimension
      order.weight = weight if weight is not None else order.weight
      order.note = note if note is not None else order.note
      order.charges = charges if charges is not None else order.charges
      order.save()
    except Order.DoesNotExist:
      raise GraphQLError({
        'message': 'Order does not exists',
        'error_code': 'ORDER_NOT_FOUND'
        })

    # Notice we return an instance of this mutation
    return UpdateOrder(order=order)


class DeleteOrder(graphene.Mutation):
  class Arguments:
    # The input arguments for this mutation
    id = graphene.ID()

  # The class attributes define the response of the mutation
  order = graphene.Field(OrderType)
  ok = graphene.Boolean()

  @login_required
  def mutate(self, info, id):
    try:
      order = Order.objects.get(pk=id)

      if order is not None:
        order.delete()
    except Order.DoesNotExist:
      raise GraphQLError({
        'message': 'Order does not exists',
        'error_code': 'ORDER_NOT_FOUND'
        })

    # Notice we return an instance of this mutation
    return DeleteOrder(order=order, ok=True)

class Mutation(graphene.ObjectType):
  create_media = CreateMedia.Field()
  update_media = UpdateMedia.Field()
  delete_media = DeleteMedia.Field()

  create_event = CreateEvent.Field()
  update_event = UpdateEvent.Field()
  delete_event = DeleteEvent.Field()

  create_order = CreateOrder.Field()
  update_order = UpdateOrder.Field()
  delete_order = DeleteOrder.Field()
