import graphene
import graphql_jwt

import accounts.schema
import contacts.schema
import business.schema
import orders.schema
import wallet.schema
import couriers.schema
import transactions.schema


class Query(accounts.schema.Query, contacts.schema.Query, business.schema.Query, orders.schema.Query, wallet.schema.Query, couriers.schema.Query, transactions.schema.Query, graphene.ObjectType):
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()


class Mutation(accounts.schema.Mutation, contacts.schema.Mutation, business.schema.Mutation, orders.schema.Mutation, wallet.schema.Mutation, couriers.schema.Mutation, transactions.schema.Mutation, graphene.ObjectType):
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()

schema = graphene.Schema(query=Query, mutation=Mutation)
