from django.db.models import Q

import graphene
from graphql import GraphQLError
from graphene import Argument
from graphene_django.types import DjangoObjectType
from graphql_jwt.decorators import login_required
from graphql_jwt.decorators import user_passes_test
from graphql_jwt.decorators import permission_required
from graphql_jwt.decorators import staff_member_required
from graphql_jwt.decorators import superuser_required

from .models import Courier
from .models import Driver
from .models import DeliveryProfile
from .models import EnumScope as _EnumScope
from accounts.models import User
from contacts.models import Address
from contacts.models import Contact
from contacts.schema import AddressInputType
from contacts.schema import ContactInputType


EnumScope = graphene.Enum.from_enum(_EnumScope)


class CourierType(DjangoObjectType):
    class Meta:
        model = Courier


class DriverType(DjangoObjectType):
    class Meta:
        model = Driver


class DeliveryProfileType(DjangoObjectType):
    class Meta:
        model = DeliveryProfile


class Query(object):
    all_couriers = graphene.List(CourierType, user=graphene.ID())
    courier = graphene.Field(CourierType, id=graphene.ID())

    all_drivers = graphene.List(
        DriverType, user=graphene.ID(), courier=graphene.ID()
    )
    driver = graphene.Field(DriverType, id=graphene.ID())

    all_delivery_profiles = graphene.List(
        DeliveryProfileType,
        courier=graphene.ID(),
        scope=graphene.Argument(EnumScope),
        origin=graphene.String(),
        destination=graphene.String(),
        active=graphene.Boolean()
    )
    delivery_profile = graphene.Field(
        DeliveryProfileType, id=graphene.ID()
    )

    @staff_member_required
    def resolve_all_couriers(self, info, user=None, **kwargs):
        if user:
            # Query Courier by user id
            return Courier.objects.filter(user=user)
        else:
            # Querying all Couriers
            return Courier.objects.all()

    @login_required
    def resolve_courier(self, info, id):
        # Querying a single courier
        return Courier.objects.get(pk=id)

    @staff_member_required
    def resolve_all_drivers(self, info, user=None, courier=None, **kwargs):

        if user or courier:
            query_result = list()

            if user is not None:
                # Query Drivers by user
                query_result = Driver.objects.filter(
                    user=user
                )
            else:
                # Query Drivers by courier
                query_result = Driver.objects.filter(
                    courier=courier
                )

            # Query Drivers by courier and user
            if courier is not None:
                query_result = query_result.filter(
                    courier=courier
                )
            return query_result

        else:
            # Querying all Drivers
            return Driver.objects.all()

    @login_required
    def resolve_driver(self, info, id):
        # Querying a single Driver
        return Driver.objects.get(pk=id)

    @staff_member_required
    def resolve_all_delivery_profiles(
        self, info, courier=None, scope=None, origin=None,
        destination=None, active=None, **kwargs
    ):
        if courier:
            # Query Delivery profile by courier
            query_result = DeliveryProfile.objects.filter(
                courier=courier
            )
            # Query Delivery profile by scope
            if scope is not None:
                query_result = query_result.filter(
                    scope=scope
                )
            # Query Delivery profile by origin
            if origin is not None:
                query_result = query_result.filter(
                    origin=origin
                )
            # Query Delivery profile by destination
            if destination is not None:
                query_result = query_result.filter(
                    destination=destination
                )
            # Query Delivery profile by active
            if active is not None:
                query_result = query_result.filter(
                    active=active
                )
            return query_result

        else:
            # Querying all Delivery profiles
            return DeliveryProfile.objects.all()

    @login_required
    def resolve_delivery_profile(self, info, id):
        # Querying a single Delivery profile
        return DeliveryProfile.objects.get(pk=id)

######################## COURIER ##########################


class CreateCourier(graphene.Mutation):
    class Arguments:
        # The input arguments for this mutation
        user = graphene.ID()
        name = graphene.String()
        contact = graphene.Argument(ContactInputType)
        address = graphene.Argument(AddressInputType)

    courier = graphene.Field(CourierType)

    @login_required
    def mutate(self, info, user, name, contact, address):
        user = User.objects.get(pk=user) if user is not None else user
        # Get or create a new address
        if address is not None:
            try:
                address = Address.objects.get(pk=address.id)
            except Address.DoesNotExist:
                address = Address.objects.create(
                    country=address.country,
                    state=address.state,
                    city=address.city,
                    zipcode=address.zipcode,
                    address_line1=address.address_line1,
                    address_line2=address.address_line2
                )
        # Get or create a new contact
        if contact is not None:
            try:
                contact = Contact.objects.get(pk=contact.id)
            except Contact.DoesNotExist:
                contact = Contact.objects.create(
                    name=contact.name,
                    address=contact.address,
                    email=contact.email
                )

        courier = Courier.objects.create(
            user=user,
            name=name,
            contact=contact,
            address=address,
        )

        return CreateCourier(
            courier=courier
        )


class UpdateCourier(graphene.Mutation):
    class Arguments:
        # The input arguments for this mutation
        id = graphene.ID()
        user = graphene.ID()
        name = graphene.String()
        contact = graphene.ID()
        address = graphene.ID()

    # The class attributes define the response of the mutation
    courier = graphene.Field(CourierType)

    @login_required
    def mutate(
        self, info, id, user=None, name=None, address=None, contact=None
    ):
        try:
            user = User.objects.get(pk=user) if user is not None else user
        except User.DoesNotExist:
            raise GraphQLError({
                'message': 'User does not exists',
                'error_code': 'USER_NOT_FOUND'
            })

        try:
            contact = Contact.objects.get(
                pk=contact) if contact is not None else contact
        except Contact.DoesNotExist:
            raise GraphQLError({
                'message': 'Contact does not exists',
                'error_code': 'CONTACT_NOT_FOUND'
            })

        try:
            address = Address.objects.get(
                pk=address) if address is not None else address
        except Address.DoesNotExist:
            raise GraphQLError({
                'message': 'Address does not exists',
                'error_code': 'ADDRESS_NOT_FOUND'
            })

        try:
            courier = Courier.objects.get(pk=id)
            courier.user = user if user is not None else courier.user
            courier.name = name if name is not None else courier.name
            courier.contact = contact if name is not None else courier.contact
            courier.address = address if address is not None else courier.address
            courier.save()
        except Courier.DoesNotExist:
            raise GraphQLError({
                'message': 'Courier does not exists',
                'error_code': 'COURIER_NOT_FOUND'
            })

        # Notice we return an instance of this mutation
        return UpdateCourier(courier=courier)


class DeleteCourier(graphene.Mutation):
    class Arguments:
        # The input arguments for this mutation
        id = graphene.ID()

    # The class attributes define the response of the mutation
    courier = graphene.Field(CourierType)
    ok = graphene.Boolean()

    @login_required
    def mutate(self, info, id):
        try:
            courier = Courier.objects.get(pk=id)
            courier.delete()
        except Courier.DoesNotExist:
            raise GraphQLError({
                'message': 'Courier does not exists',
                'error_code': 'COURIER_NOT_FOUND'
            })

        # Notice we return an instance of this mutation
        return DeleteCourier(courier=courier, ok=True)


############################ DRIVER ##############################
class CreateDriver(graphene.Mutation):
    class Arguments:
        # The input arguments for this mutation
        user = graphene.ID()
        courier = graphene.ID()

    driver = graphene.Field(DriverType)

    @login_required
    def mutate(self, info, user, courier):
        # Get user object
        try:
            user = User.objects.get(pk=user)
        except User.DoesNotExist:
            raise GraphQLError({
                'message': 'User does not exists',
                'error_code': 'USER_NOT_FOUND'
            })

        # Get courier object
        try:
            courier = Courier.objects.get(pk=courier)
        except Courier.DoesNotExist:
            raise GraphQLError({
                'message': 'Courier does not exists',
                'error_code': 'COURIER_NOT_FOUND'
            })

        driver = Driver.objects.create(
            user=user,
            courier=courier
        )

        return CreateDriver(
            driver=driver
        )


class UpdateDriver(graphene.Mutation):
    class Arguments:
        # The input arguments for this mutation
        id = graphene.ID()
        courier = graphene.ID()

    driver = graphene.Field(DriverType)

    @login_required
    def mutate(self, info, id, courier):
        # Get courier object
        try:
            courier = Courier.objects.get(pk=courier)
        except Courier.DoesNotExist:
            raise GraphQLError({
                'message': 'Courier does not exists',
                'error_code': 'COURIER_NOT_FOUND'
            })

        # Get driver object
        try:
            driver = Driver.objects.get(pk=id)
            driver.courier = courier if courier is not None else driver.courier
            driver.save()
        except Driver.DoesNotExist:
            raise GraphQLError({
                'message': 'Driver does not exists',
                'error_code': 'DRIVER_NOT_FOUND'
            })

        return UpdateDriver(
            driver=driver
        )


class DeleteDriver(graphene.Mutation):
    class Arguments:
        # The input arguments for this mutation
        id = graphene.ID()

    # The class attributes define the response of the mutation
    ok = graphene.Boolean()
    driver = graphene.Field(DriverType)

    @login_required
    def mutate(self, info, id):
        try:
            driver = Driver.objects.get(pk=id)
            driver.delete()
        except Driver.DoesNotExist:
            raise GraphQLError({
                'message': 'Driver does not exists',
                'error_code': 'DRIVER_NOT_FOUND'
            })

        # Notice we return an instance of this mutation
        return DeleteDriver(ok=True)


################### DELIVERY PROFILE ##########################
class CreateDeliveryProfile(graphene.Mutation):
    class Arguments:
        # The input arguments for this mutation
        title = graphene.String()
        courier = graphene.ID()
        origin = graphene.String()
        destination = graphene.String()
        scope = graphene.Argument(EnumScope)
        price = graphene.Float()
        active = graphene.Boolean()

    delivery_profile = graphene.Field(DeliveryProfileType)

    @login_required
    def mutate(
        self, info, courier, origin, destination,
        scope, price, active=False, title=None
    ):
        try:
            courier = Courier.objects.get(pk=courier)
        except Courier.DoesNotExist:
            raise GraphQLError({
                'message': 'Courier does not exists',
                'error_code': 'COURIER_NOT_FOUND'
            })

        delivery_profile_instance = DeliveryProfile.objects.create(
            courier=courier,
            title=title,
            origin=origin,
            destination=destination,
            scope=scope,
            price=price,
            active=active
        )

        return CreateDeliveryProfile(
            delivery_profile=delivery_profile_instance
        )


class UpdateDeliveryProfile(graphene.Mutation):
    class Arguments:
        # The input arguments for this mutation
        id = graphene.ID()
        title = graphene.String()
        courier = graphene.ID()
        origin = graphene.String()
        destination = graphene.String()
        scope = graphene.Argument(EnumScope)
        price = graphene.Float()
        active = graphene.Boolean()

    delivery_profile = graphene.Field(DeliveryProfileType)

    @login_required
    def mutate(
        self, info, id, courier=None, origin=None, destination=None,
        scope=None, price=None, active=None, title=None
    ):
        try:
            delivery_profile_instance = DeliveryProfile.objects.get(pk=id)
        except DeliveryProfile.DoesNotExist:
            raise GraphQLError({
                'message': 'DeliveryProfile does not exists',
                'error_code': 'DELIVERY_PROFILE_NOT_FOUND'
            })

        if courier is not None:
            try:
                courier = Courier.objects.get(pk=courier)
            except Courier.DoesNotExist:
                raise GraphQLError({
                    'message': 'Courier does not exists',
                    'error_code': 'COURIER_NOT_FOUND'
                })

        delivery_profile_instance.courier = courier if courier is not None else delivery_profile_instance.courier
        delivery_profile_instance.origin = origin if origin is not None else delivery_profile_instance.origin
        delivery_profile_instance.destination = destination if destination is not None else delivery_profile_instance.destination
        delivery_profile_instance.scope = scope if scope is not None else delivery_profile_instance.scope
        delivery_profile_instance.price = price if price is not None else delivery_profile_instance.price
        delivery_profile_instance.active = active if active is not None else delivery_profile_instance.active
        delivery_profile_instance.title = title if title is not None else delivery_profile_instance.title

        delivery_profile_instance.save()

        return UpdateDeliveryProfile(
            delivery_profile=delivery_profile_instance
        )


class DeleteDeliveryProfile(graphene.Mutation):
    ok = graphene.Boolean()

    class Arguments:
        id = graphene.ID()

    @login_required
    def mutate(self, info, id):
        try:
            delivery_profile = DeliveryProfile.objects.get(pk=id)
            delivery_profile.delete()
        except DeliveryProfile.DoesNotExist:
            raise GraphQLError({
                'message': 'DeliveryProfile does not exists',
                'error_code': 'DELIVERY_PROFILE_NOT_FOUND'
            })

        return DeleteDeliveryProfile(
            ok=True
        )


class Mutation(graphene.ObjectType):
    create_courier = CreateCourier.Field()
    update_courier = UpdateCourier.Field()
    delete_courier = DeleteCourier.Field()

    create_driver = CreateDriver.Field()
    update_driver = UpdateDriver.Field()
    delete_driver = DeleteDriver.Field()

    create_delivery_profile = CreateDeliveryProfile.Field()
    update_delivery_profile = UpdateDeliveryProfile.Field()
    delete_delivery_profile = DeleteDeliveryProfile.Field()
