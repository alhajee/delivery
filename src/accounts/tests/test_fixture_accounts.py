import pytest
from django.test import TestCase
from mixer.backend.django import mixer
from graphene.test import Client
 
from accounts.models import User
from accounts.models import Qualification
from accounts.models import NextOfKin
from accounts.models import WorkExperience
from accounts.models import Biodata 
from accounts import schema


auth_query = """
    mutation {
        tokenAuth(email:"phuckmanity@gmail.com", password:"P4ssw0rd"){
            refreshExpiresIn
            token
        }
    }
"""

user_list_query = """
    query {
        allUsers {
            id
            firstName
            middleName
            lastName
            email
            isStaff
            accountType
        }
    }
"""
 
single_user_query = """
    query($id:ID!) {
        user(id:$id) {
            id
            firstName
            middleName
            lastName
            email
            isStaff
            accountType
        }
    }
"""
 
create_user_mutation = """
    mutation CreateUser($input: UserInputType!) {
        createUser(input: $input){
            user {
                id
                firstName
                middleName
                lastName
                email
                isStaff
                accountType
            }
            ok
        }
    }
"""
 
update_user_mutation = """
     mutation UpdateUser($input: UserInputType!) {
        updateUser(input: $input) {
            user {
                id
                firstName
                middleName
                lastName
                email
                isStaff
                accountType
            }
            ok
        }
    }
"""
 
delete_user_mutation = """
    mutation DeleteBlog($input: DeleteBlogInputType!) {
        deleteBlog(input: $input) {
            ok
        }
    }
"""

@pytest.mark.django_db
class TestUserSchema(TestCase):
 
    def setUp(self):
        self.client = Client(schema)
        self.user = mixer.blend(User)
 
    def test_single_user_query(self):
        response = self.client.execute(single_user_query, variables={"id": self.user.id})
        response_user = response.get("data").get("user")
        assert response_user["id"] == str(self.user.id)
 
 
    def test_user_list_query(self):
        mixer.blend(User)
 
        response = self.client.execute(user_list_query)
        users = response.get("data").get("users")
        ok = response.get("data").get("ok")
         
        assert len(users)
 
    def test_create_user(self):
        author = mixer.blend(User)
        payload = {
            "title": "How to test GraphQL with pytest",
            "authorId": author.id,
            "body": "This is the example of functional testing.",
        }
 
        response = self.client.execute(create_user_mutation, variables={"input": payload})
        user = response.get("data").get("createUser").get("user")
        title = user.get("title")
        assert title == payload["title"]
 
    def test_update_user(self):
        payload = {
            "id": self.user.id,
            "title": "How to test GraphQL update mutation with pytest"
        }
 
        response = self.client.execute(update_user_mutation, variables={"input": payload})
 
        response_user = response.get("data").get("updateUser").get("user")
        title = response_user.get("title")
        assert title == payload["title"]
        assert title != self.user.title 
 
    def test_delete_account(self):
        payload = {
            "id": self.user.id
        }
        response = self.client.execute(delete_user_mutation, variables={"input": payload})
        ok = response.get("data").get("deleteUser").get("ok")
        assert ok