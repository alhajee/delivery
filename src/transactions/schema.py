import graphene
from graphene import Argument
from graphene_django.types import DjangoObjectType

from graphql_jwt.decorators import login_required
from graphql_jwt.decorators import user_passes_test
from graphql_jwt.decorators import permission_required
from graphql_jwt.decorators import staff_member_required
from graphql_jwt.decorators import superuser_required
from graphql import GraphQLError
from django.db.models import Q

from .models import Transaction
from .models import EnumPaymentMethod as _EnumPaymentMethod
from .models import EnumTransactionStatus as _EnumTransactionStatus

from accounts.models import User
from orders.models import Order


EnumPaymentMethod = graphene.Enum.from_enum(_EnumPaymentMethod)
EnumTransactionStatus = graphene.Enum.from_enum(_EnumTransactionStatus)


class TransactionType(DjangoObjectType):
    class Meta:
        model = Transaction


class Query(object):
    all_transactions = graphene.List(TransactionType, user=graphene.ID())
    transaction = graphene.Field(TransactionType, id=graphene.ID())

    @login_required
    def resolve_all_transactions(self, info, user=None, **kwargs):
        if user is not None:
            return Transaction.objects.filter(user=user)
        else:
            # Querying list of all transactions
            return Transaction.objects.all()

    @login_required
    def resolve_transaction(self, info, id):
        # Querying a single event
        return Transaction.objects.get(pk=id)


######################## TRANSACTION ##########################
class CreateTransaction(graphene.Mutation):
    class Arguments:
        user = graphene.ID()
        order = graphene.ID()
        amount = graphene.Float()
        status = graphene.Argument(
            EnumTransactionStatus
        )
        payment_method = graphene.Argument(
            EnumPaymentMethod
        )

    transaction = graphene.Field(TransactionType)
    ok = graphene.Boolean()

    @login_required
    def mutate(self, info, user, amount, status, order=None, payment_method=None):
    
        try:
            user = User.objects.get(pk=user)
        except User.DoesNotExist:
            raise GraphQLError({
                'message': 'User does not exists',
                'error_code': 'USER_NOT_FOUND'
            })

        if order:
            try:
                order = Order.objects.get(pk=order)
            except Order.DoesNotExist:
                raise GraphQLError({
                    'message': 'Order does not exists',
                    'error_code': 'ORDER_NOT_FOUND'
                })

        transaction = Transaction.objects.create(
            user=user,
            amount=amount,
            status=status,
            order=order,
            payment_method=payment_method
        )

        return CreateTransaction(
            transaction=transaction, ok=True
        )

class UpdateTransaction(graphene.Mutation):
    class Arguments:
        id = graphene.ID()
        user = graphene.ID()
        order = graphene.ID()
        amount = graphene.Float()
        status = graphene.Argument(
            EnumTransactionStatus
        )
        payment_method = graphene.Argument(
            EnumPaymentMethod
        )

    transaction = graphene.Field(TransactionType)
    ok = graphene.Boolean()

    @login_required
    def mutate(self, info, id, user=None, amount=None, status=None, order=None, payment_method=None):
    
        try:
            transaction = Transaction.objects.get(pk=id)
        except Transaction.DoesNotExist:
            raise GraphQLError({
                'message': 'Transaction does not exists',
                'error_code': 'TRANSACTION_NOT_FOUND'
            })

        if user is not None:
            try:
                user = User.objects.get(pk=user)
            except User.DoesNotExist:
                raise GraphQLError({
                    'message': 'User does not exists',
                    'error_code': 'USER_NOT_FOUND'
                })

        if order is not None:
            try:
                order = Order.objects.get(pk=order)
            except Order.DoesNotExist:
                raise GraphQLError({
                    'message': 'Order does not exists',
                    'error_code': 'ORDER_NOT_FOUND'
                })

        transaction.user = user if user is not None else transaction.user
        transaction.order = order if order is not None else transaction.order
        transaction.amount = amount if amount is not None else transaction.amount
        transaction.status = status if status is not None else transaction.status
        transaction.payment_method = payment_method if payment_method is not None else transaction.payment_method
        transaction.save()
        # Notice we return an instance of this mutation
        return UpdateTransaction(
            transaction=transaction,
            ok=True
        )

class DeleteTransaction(graphene.Mutation):
    class Arguments:
        # The input arguments for this mutation
        id = graphene.ID()

    # The class attributes define the response of the mutation
    transaction = graphene.Field(TransactionType)
    ok = graphene.Boolean()

    @login_required
    def mutate(self, info, id):
        try:
            transaction = Transaction.objects.get(pk=id)
            transaction.delete()
        except Transaction.DoesNotExist:
            raise GraphQLError({
                'message': 'Transaction does not exists',
                'error_code': 'TRANSACTION_NOT_FOUND'
            })

        # Notice we return an instance of this mutation
        return DeleteTransaction(
            transaction=transaction,
            ok=True
        )


class Mutation(graphene.ObjectType):
    create_transaction = CreateTransaction.Field()
    update_transaction = UpdateTransaction.Field()
    delete_transaction = DeleteTransaction.Field()