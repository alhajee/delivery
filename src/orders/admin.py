from django.contrib import admin

from .models import Order, Media, Event

admin.site.register(Order)
admin.site.register(Media)
admin.site.register(Event)

