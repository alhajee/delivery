from django.db.models import Q
from django.db.models import Q

import graphene
from graphql import GraphQLError
from graphene import Argument
from graphene_django.types import DjangoObjectType
from graphql_jwt.decorators import login_required
from graphql_jwt.decorators import user_passes_test
from graphql_jwt.decorators import permission_required
from graphql_jwt.decorators import staff_member_required
from graphql_jwt.decorators import superuser_required

from .models import Address
from .models import Contact

class AddressInputType(graphene.InputObjectType):
    id = graphene.ID()
    country = graphene.String()
    state = graphene.String()
    city = graphene.String()
    zipcode = graphene.String()
    address_line1 = graphene.String()
    address_line2 = graphene.String()


class ContactInputType(graphene.InputObjectType):
    id = graphene.ID()
    name = graphene.String()
    phone = graphene.String()
    address = graphene.Argument(AddressInputType)
    email = graphene.String()


class AddressType(DjangoObjectType):
    class Meta:
        model = Address
        description = " Type definition for a single address "
        filter_fields = {
            "id": ("exact", ),
            "country": ("iexact"),
            "state": ("iexact"),
            "city": ("icontains", "iexact"),
            "zipcode": ("iexact"),
            "address_line1": ("icontains", ),
            "address_line2": ("icontains", ),
        }


class ContactType(DjangoObjectType):
    class Meta:
        model = Contact


class Query(object):
    all_addresses = graphene.List(AddressType)
    address = graphene.Field(AddressType, id=graphene.ID())

    all_contacts = graphene.List(ContactType)
    contact = graphene.Field(ContactType, id=graphene.ID())
    
    @staff_member_required
    def resolve_all_addresses(self, info, **kwargs):
        # Querying a list
        return Address.objects.all()

    @login_required
    def resolve_address(self, info, id):
        # Querying a single address
        return Address.objects.get(pk=id)

    @staff_member_required
    def resolve_all_contacts(self, info, **kwargs):
        # Querying a list
        return Contact.objects.all()

    @login_required
    def resolve_contact(self, info, id):
        # Querying a single contact
        return Contact.objects.get(pk=id)


########################ADDRESS##########################
class CreateAddress(graphene.Mutation):
    class Arguments:
        country = graphene.String()
        state = graphene.String()
        city = graphene.String()
        zipcode = graphene.String()
        address_line1 = graphene.String()
        address_line2 = graphene.String()

    address = graphene.Field(AddressType)

    def mutate(self, info, country, state, city, address_line1, zipcode=None, address_line2=None):
        try:
            address = Address.objects.create(
                country=country,
                state=state,
                city=city,
                zipcode=zipcode,
                address_line1=address_line1,
                address_line2=address_line2
            )
        except Address.DoesNotExist:
            raise GraphQLError({
                'message': 'Address does not exists',
                'error_code': 'ADDRESS_NOT_FOUND'
                })
       
        return CreateAddress(
            address=address
        )


class UpdateAddress(graphene.Mutation):
    class Arguments:
        # The input arguments for this mutation
        id = graphene.ID()
        country = graphene.String()
        state = graphene.String()
        city = graphene.String()
        zipcode = graphene.String()
        address_line1 = graphene.String()
        address_line2 = graphene.String()

    # The class attributes define the response of the mutation
    address = graphene.Field(AddressType)

    @login_required
    def mutate(self, info, id, country=None, state=None, city=None, zipcode=None, address_line1=None, address_line2=None):
        try:
            address = Address.objects.get(pk=id)
            address.country = country if country is not None else address.country
            address.state = state if state is not None else address.state
            address.city = city if city is not None else address.city
            address.zipcode = zipcode if zipcode is not None else address.zipcode
            address.address_line1 = address_line1 if address_line1 is not None else address.address_line1
            address.address_line2 = address_line2 if address_line1 is not None else address.address_line2
            address.save()
        except Address.DoesNotExist:
            raise GraphQLError({
                'message': 'Address does not exists',
                'error_code': 'ADDRESS_NOT_FOUND'
                })

        # Notice we return an instance of this mutation
        return UpdateAddress(address=address)


class DeleteAddress(graphene.Mutation):
    class Arguments:
        # The input arguments for this mutation
        id = graphene.ID()

    # The class attributes define the response of the mutation
    address = graphene.Field(AddressType)
    ok = graphene.Boolean()

    @login_required
    def mutate(self, info, id):
        try:
            address = Address.objects.get(pk=id)

            if address is not None:
                address.delete()
                address.save()
        except Address.DoesNotExist:
            raise GraphQLError({
                'message': 'Address does not exists',
                'error_code': 'ADDRESS_NOT_FOUND'
                })

        # Notice we return an instance of this mutation
        return DeleteAddress(address=address, ok=True)


########################CONTACT##########################
class CreateContact(graphene.Mutation):

    contact = graphene.Field(ContactType)

    class Arguments:
        name = graphene.String()
        phone = graphene.String()
        address = graphene.Argument(AddressInputType)
        email = graphene.String()
        # The input arguments for this mutation


    @login_required
    def mutate(self, info, name, phone, address, email):
        try:
            address, created = Address.objects.filter(Q(pk=address.id)).get_or_create(
                country=address.country,
                state=address.state,
                city=address.city,
                zipcode=address.zipcode,
                address_line1=address.address_line1,
                address_line2=address.address_line2
            )
            
            contact = Contact.objects.create(
                name=name, phone=phone, address=address, email=email
                )
        except Contact.DoesNotExist:
            raise GraphQLError({
                'message': 'Contact does not exists',
                'error_code': 'CONTACT_NOT_FOUND'
                })

        return CreateContact(
            contact=contact
        )


class UpdateContact(graphene.Mutation):
    class Arguments:
        # The input arguments for this mutation
        id = graphene.ID()
        name = graphene.String()
        phone = graphene.String()
        address = graphene.ID()
        email = graphene.String()

    # The class attributes define the response of the mutation
    contact = graphene.Field(ContactType)

    @login_required
    def mutate(self, info, id, name=None, phone=None, address=None, email=None):
        try:
            address = Address.objects.get(
                pk=address) if address is not None else address
        except Address.DoesNotExist:
            raise GraphQLError({
                'message': 'Address does not exists',
                'error_code': 'ADDRESS_NOT_FOUND'
                })

        try:
            contact = Contact.objects.get(pk=id)
            contact.name = name if name is not None else contact.name
            contact.phone = phone if phone is not None else contact.phone
            contact.address = address if address is not None else contact.address
            contact.email = email if email is not None else contact.email
            contact.save()
        except Contact.DoesNotExist:
            raise GraphQLError({
                'message': 'Contact does not exists',
                'error_code': 'CONTACT_NOT_FOUND'
                })

        # Notice we return an instance of this mutation
        return UpdateContact(contact=contact)


class DeleteContact(graphene.Mutation):
    class Arguments:
        # The input arguments for this mutation
        id = graphene.ID()

    # The class attributes define the response of the mutation
    contact = graphene.Field(ContactType)
    ok = graphene.Boolean()
    
    @login_required
    def mutate(self, info, id):
        try:
            contact = Contact.objects.get(pk=id)

            if contact is not None:
                contact.delete()
                contact.save()
        except Contact.DoesNotExist:
            raise GraphQLError({
                'message': 'Contact does not exists',
                'error_code': 'CONTACT_NOT_FOUND'
                })

        # Notice we return an instance of this mutation
        return DeleteContact(contact=contact, ok=True)


class Mutation(graphene.ObjectType):
    create_address = CreateAddress.Field()
    update_address = UpdateAddress.Field()
    delete_address = DeleteAddress.Field()

    create_contact = CreateContact.Field()
    update_contact = UpdateContact.Field()
    delete_contact = DeleteContact.Field()
