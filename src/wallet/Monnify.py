
import os
import requests
import re
import json
import datetime
import random
import calendar
import base64
import hashlib
import urllib

from requests.auth import HTTPDigestAuth
from requests.auth import HTTPBasicAuth
from requests.exceptions import ConnectionError
from requests.exceptions import HTTPError
from requests.exceptions import Timeout

from Delivery.settings import SANDBOX, env

MONNIFY_APIKEY = os.environ.get(
    'MONNIFY_SANDBOX_APIKEY') if SANDBOX == True else env('MONNIFY_APIKEY')
MONNIFY_SECRETKEY = os.environ.get(
    'MONNIFY_SANDBOX_SECRETKEY') if SANDBOX == True else env('MONNIFY_SECRETKEY')
MONNIFY_ENDPOINT = os.environ.get(
    'MONNIFY_SANDBOX_ENDPOINT') if SANDBOX == True else env('MONNIFY_ENDPOINT')
MONNIFY_CONTRACT_CODE = os.environ.get(
    'MONNIFY_SANDBOX_CONTRACT_CODE') if SANDBOX == True else env('MONNIFY_CONTRACT_CODE')
MONNIFY_QUERY = os.environ.get(
    'MONNIFY_SANDBOX_QUERY') if SANDBOX == True else env('MONNIFY_QUERY')

def webhook(data=None):

    monnify = Monnify()

    """{
        "transactionReference":"MNFY|20200803005343|002655",
        "paymentReference":"MNFY|20200803005343|002655",
        "amountPaid":"1000.00",
        "totalPayable":"1000.00",
        "settlementAmount":"990.00",
        "paidOn":"03/08/2020 12:53:44 AM",
        "paymentStatus":"PAID",
        "paymentDescription":"Haruna Muhd",
        "transactionHash":"5dc06450e925a1d8691b252240bdfd67e4fc1013b256026c7294d25ea87304ee9530b4b4f641a2c1a55853b6e9c037a6dfb9e4551cae6cf70b465bf95afce5f8",
        "currency":"NGN",
        "paymentMethod":"ACCOUNT_TRANSFER",
        "product":{
            "type":"RESERVED_ACCOUNT",
            "reference":"y9DLnFSYRCeAKMrNKg7jw6YVbIT2"
        },
        "cardDetails":"None",
        "accountDetails":{
            "accountName":"Monnify Limited",
            "accountNumber":"******2190",
            "bankCode":"001",
            "amountPaid":"1000.00"
        },
        "accountPayments":[
            {
            "accountName":"Monnify Limited",
            "accountNumber":"******2190",
            "bankCode":"001",
            "amountPaid":"1000.00"
            }
        ],
        "customer":{
            "email":"testtester04834@outlook.com",
            "name":"Haruna Muhd"
        },
        "metaData":{

        }
        }"""

    transaction = {

    }

    print("Received Data: ", data)

    transaction["transaction_ref"] = data.get("transactionReference")
    transaction["payment_ref"] = data.get("paymentReference")
    transaction["amount"] = data.get("amountPaid")
    transaction["total_amount"] = data.get("totalPayable")
    transaction["payment_date"] = data.get("paidOn")
    transaction["payment_status"] = data.get("paymentStatus")
    transaction["payment_describtion"] = data.get("paymentDescription")
    transaction["transaction_hash"] = data.get("transactionHash")
    transaction["currency"] = data.get("currency")
    transaction["payment_method"] = data.get("paymentMethod")
    transaction["product_ref"] = data.get("product").get("reference")
    transaction["account_name"] = data.get(
        "accountDetails").get("accountName")
    transaction["account_number"] = data.get(
        "accountDetails").get("accountNumber")
    transaction["bank_code"] = data.get("accountDetails").get("bankCode")
    transaction["amount_paid"] = data.get(
        "accountDetails").get("amountPaid")
    transaction["customer_name"] = data.get("customer").get("name")
    transaction["customer_email"] = data.get("customer").get("email")

    hash = cal_transaction_hash(clientSecret=monnify.CLIENT_SECRET, paymentReference=transaction["payment_ref"],
                                amountPaid=transaction["amount"], paidOn=transaction["payment_date"], transactionReference=transaction["transaction_ref"])

    if hash == transaction["transaction_hash"]:
        query = monnify.query_transaction(
            transaction_ref=transaction["transaction_ref"])

        if "requestSuccessful" in str(query):
            if query.get("requestSuccessful", False) and query.get("responseMessage") == "success":
                transaction_details = query.get("responseBody")
                transaction_status = transaction_details.get("paymentStatus")
                settlement_amount = transaction_details.get("settlementAmount")

                if transaction_status == "PAID":
                    print(transaction)
                    # TODO: get user with transaction["product_ref"]

            else:
                # TODO: handle this
                pass
        else:
             # TODO: handle this
            pass

    else:
         # TODO: handle this
        pass


def cal_transaction_hash(clientSecret=None, paymentReference=None, amountPaid=None, paidOn=None, transactionReference=None):
    # {clientSecret} | {paymentReference} | {amountPaid} | {paidOn} | {transactionReference}

    pattern = f"{clientSecret}|{paymentReference}|{amountPaid}|{paidOn}|{transactionReference}"

    hash = hashlib.sha512(bytes(pattern, encoding="ascii")).hexdigest()

    print("Hash: ", hash)

    return hash


class Monnify:
    def __init__(self, sub_account=None):
        self.SUB_ACCOUNT = sub_account
        self.API_KEY = MONNIFY_APIKEY
        self.CONTRACT_CODE = MONNIFY_CONTRACT_CODE
        self.CLIENT_SECRET = MONNIFY_SECRETKEY
        self.TOKEN = ""
        self.ENDPOINT = MONNIFY_ENDPOINT
        self.CURRENCY = "NGN"
        self.AUTH_ENDPOINT = self.ENDPOINT + "/auth/login"
        self.BASIC_AUTH = base64.standard_b64encode(
            bytes(f"{self.API_KEY}:{self.CLIENT_SECRET}", encoding="ascii"))
        """
            POST https://sandbox.monnify.com/api/v1/auth/login
            Base64 apiKey:clientSecret
            {
                "requestSuccessful": true,
                "responseMessage": "success",
                "responseCode": "0",
                "responseBody": {
                    "accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
                    "expiresIn": 3599
                }
            }

        """
        print("before auth")
        status, response = api_request(
            endpoint=self.AUTH_ENDPOINT, auth=BasicAuth(self.BASIC_AUTH))
        print("MONNIFY AUTH: ", response)

        if status["ok"]:
            if "accessToken" in str(response):
                self.TOKEN = response.get("responseBody").get("accessToken")

    def create_reserved_account(self, account_name=None, account_bvn=None, email=None, account_ref=None, currency=None):
        currency = currency if currency else self.CURRENCY
        endpoint = "{}/bank-transfer/reserved-accounts".format(self.ENDPOINT)
        payload = {
            "accountReference": account_ref,
            "accountName": account_name,
            "currencyCode": currency,
            "contractCode": self.CONTRACT_CODE,
            "customerEmail": email,
            "customerName": account_name,
            "restrictPaymentSource": False,
        }

        print("Payload: ", payload)
        print("Token: ", self.TOKEN)
        status, response = api_request(
            endpoint=endpoint, auth=BearerAuth(self.TOKEN), payload=json.dumps(payload), headers={'Content-type': 'application/json'})

        if status["ok"]:

            if "requestSuccessful" in response and "responseMessage" in response:
                if response.get("requestSuccessful", False) and response.get("responseMessage") == "success":
                    account_details = response.get("responseBody")
                    print("Acount Details: ", account_details)
                    return account_details
                else:
                    return {"ok": False, "message": "CREATING_RESERVED_ACCOUNT_FAILED"}

            else:
                return {"ok": False, "message": "CREATING_RESERVED_ACCOUNT_FAILED"}

        else:
            return response

    def query_transaction(self, transaction_ref=None):
        endpoint = f"{MONNIFY_QUERY}/{urllib.parse.quote(transaction_ref)}"

        status, response = api_request(
            endpoint=endpoint, auth=BearerAuth(self.TOKEN), method="GET")

        return response


class BearerAuth(requests.auth.AuthBase):
    def __init__(self, token):
        self.token = token

    def __call__(self, r):
        r.headers["authorization"] = "Bearer " + self.token
        return r


class TokenAuth(requests.auth.AuthBase):
    def __init__(self, token):
        self.token = token

    def __call__(self, r):
        r.headers["authorization"] = "Token " + self.token
        return r


class BasicAuth(requests.auth.AuthBase):
    def __init__(self, key):
        self.key = key

    def __call__(self, r):
        r.headers["authorization"] = "Basic " + self.key.decode("utf-8")
        return r


def api_request(endpoint=None, payload=None, auth=None, headers=None, method=None):
    print("API REQUEST")
    response = {
        "ok": False,
    }

    try:

        print(payload)

        if method == "GET":
            re = requests.get(endpoint, auth=auth,
                              params=payload)
            print("API REQUEST: ", re)

        else:
            re = requests.post(endpoint, auth=auth,
                               data=payload, headers=headers)
            print("API REQUEST: ", re)

        # re.raise_for_status()
        print("content Type: ", re.headers.get('content-type'))

        accepted_status_code = [200, 201]

        if re.status_code in accepted_status_code and "application/json" in re.headers.get('content-type'):

            return ({"ok": True}, re.json())

        else:

            print("API: Http error", re.text)

            response["message"] = "EXT_SYSTEM_RESPONSE_ERROR"

            return ({"ok": False}, response)

    except requests.exceptions.HTTPError as httpErr:
        print("API: HttpError ", httpErr)

        response["message"] = "EXT_SYSTEM_HTTP_ERROR"

        return ({"ok": False}, response)
    except requests.exceptions.ConnectionError as connErr:
        print("API: ConnectionError ", connErr)

        response["message"] = "EXT_CONNECTION_ERROR"

        return ({"ok": False}, response)

    except requests.exceptions.Timeout as timeOutErr:
        print("API: TimeoutError ", timeOutErr)

        response["message"] = "EXT_SYSTEM_TIMEOUT_ERROR"

        return ({"ok": False}, response)

    except requests.exceptions.RequestException as reqErr:
        print("API: Request Error ", reqErr)

        response["message"] = "EXT_SYSTEM_REQUEST_ERROR"

        return ({"ok": False}, response)

    except Exception as e:
        print("API_REQUEST: ", e)

        response["message"] = "EXT_SYSTEM_UNKNOWN_ERROR"

        return ({"ok": False}, response)
