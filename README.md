# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)



# How do I get set up? ###


## Deploying In Docker container
### For development
To create a fresh build and run the app in a docker container
```bash
docker-compose -f docker-compose.dev.yml up --build
```

To run the docker container without creating a fresh build, 
simply omit `--build` from the command

#### For Production ####

```bash
docker-compose -f docker-compose.prod.yml up --build
```

## Authentication
Most of the commands in the API requires a user to be authenticated

### Using API-TOKEN-AUTH
* Send a post request to api-token-auth to generate authorization token `http POST http://localhost:8000/api-token-auth/ username={username} password={password}`
* use the generated authentication token as part of the request to get response `http GET  http://localhost:8000/api/contacts/ "Authorization: Token {token}"`
* in `settings.py` change `DEFAULT_AUTHENTICATION_CLASSES` to `rest_framework.authentication.SessionAuthentication` for session based authentication
* or `rest_framework.authentication.TokenAuthentication` for token based authentication

### Authentication using GraphQL API
```graphql
{
  tokenAuth(email:"<registered email>", password:"<password>") {
    token
  }
}
```

## Creating new user using GraphQL API
```graphql
mutation {
  createUser(firstName:"<first name>", lastName:"<last name>", email:"<email address>", password:"<password>", accountType:<PERSONAL/BUSINESS>){
    ok
  }
}
```


### Visualizing models using graphviz

* Create a dot file `./manage.py graph_models -a > delivery.dot`
* Create a PNG image file called my_project_visualized.png with application grouping `./manage.py graph_models --pydot -a -g -o delivery_visualized.png`

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact