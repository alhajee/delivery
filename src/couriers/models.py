from django.db import models
from django.utils import timezone
from django.conf import settings
from django.dispatch import receiver
from django.contrib.auth.models import User
from django.utils.translation import gettext as _

from accounts.models import (
    User,
    EnumAccountType
)
from accounts.signals import user_created
from contacts.models import Address, Contact
from business.models import Business


class EnumScope(models.TextChoices):
    COUNTRY = 'COUNTRY', 'Country'
    STATE = 'STATE', 'State'
    CITY = 'CITY', 'City'
    OTHERS = 'OTHERS', 'Others'
    ALL = 'ALL', 'All'


class BaseTimestampModel(models.Model):
    created_at = models.DateTimeField(
        verbose_name=u"Created at",
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        verbose_name=u"Updated at",
        auto_now=True
    )

    class Meta:
        abstract = True


class Courier(BaseTimestampModel):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE
    )
    name = models.CharField(_("Company Name"), max_length=50)
    contact = models.ForeignKey(
        Contact,
        verbose_name=_("Contact person"),
        on_delete=models.DO_NOTHING
    )
    address = models.ForeignKey(
        Address,
        verbose_name=_("Company address"),
        on_delete=models.DO_NOTHING
    )

    def __str__(self):
        return self.name


class Driver(BaseTimestampModel):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE
    )
    courier = models.ForeignKey(
        Courier, on_delete=models.CASCADE, blank=True, null=True
    )

    def __str__(self):
        if self.courier:
            return f"{self.user.get_full_name()} - {self.courier.name}"
        else:
            return f"{self.user.get_full_name()}"

class DeliveryProfile(BaseTimestampModel):
    title = models.CharField(
        max_length=50, null=True, blank=True
    )
    courier = models.ForeignKey(
        Courier, on_delete=models.CASCADE
    )
    origin = models.CharField(
        max_length=50, default="ALL"
    )
    destination = models.CharField(
        max_length=50, default="ALL"
    )
    scope = models.CharField(
        max_length=50,
        choices=EnumScope.choices,
        default=EnumScope.ALL
    )
    price = models.DecimalField(
        _("Minimum Price"), max_digits=9, decimal_places=2
    )
    active = models.BooleanField(
        default=True
    )

    def __str__(self):
        return self.title


@receiver(user_created, sender=User)
def handle_user_created(sender, **kwargs):
    """
    Event handler that gets griggered whenever 
    a user_created signal is received
    """
    user = kwargs['user']
    response = create_driver(user=user, courier=None)
    print(response)


def create_driver(user:User, courier:Courier)->Driver:
    """ Creates a driver instance if an account type is DRIVER

    Args:
        user (User): An instance of a user model
        courier (Courier): An instance of a courier model

    Returns:
        Driver: An instance of a driver model
    """
    if user.account_type == EnumAccountType.DRIVER:
        try:
            driver = Driver.objects.create(
                user=user,
                courier=courier
            )
            driver.save()
            print("driver profile successfully created")
            return driver
        except Exception as e:
            print("failed to create driver profile")
            print(e)

    return None
