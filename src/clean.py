import os
import shutil
import glob
from os import path
from Delivery.settings import INSTALLED_APPS


PROJECT_NAME = "Delivery"

apps = []

# Check apps that exist inn project directory
for app in INSTALLED_APPS:
    if path.exists(app):
        apps.append(app)

def delete_pycache(app):
    directory = path.join(app, "__pycache__")
    
    print(directory)
    
    if path.exists(directory):
        shutil.rmtree(directory)
        os.rmdir(directory)
        # Check if operationo was successful
        if not path.exists(directory):
            print("successful")
        else:
            print("failed")

def delete_migrations(app):
    directory = path.join(app, "migrations")
    
    print(directory)
    
    if path.exists(directory):
        for CleanUp in glob.glob(path.join(directory, "*.*")):
            print(CleanUp)
            if not CleanUp.endswith('__init__.py'):    
                os.remove(CleanUp)
    


for app in apps:
    delete_pycache(app)    
    delete_migrations(app)
#    delete "__pycache__" in migrations



