from django.shortcuts import render
from django.core import serializers
from .models import Business, Address, Contact
from django.core import serializers
from django.core.serializers.json import DjangoJSONEncoder


class LazyEncoder(DjangoJSONEncoder):
    def default(self, obj):
        if isinstance(obj, YourCustomType):
            return str(obj)
        return super().default(obj)


def model_serializer():
  with open("contacts.json", "w") as out:
    serializers.serialize("json", Contact.objects.all(), cls=LazyEncoder, stream=out)
  print("serialized")
  return 0