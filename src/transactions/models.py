from django.db import models

from accounts.models import User
from orders.models import Order


class EnumTransactionStatus(models.TextChoices):
    NOT_PAID = 'NOT_PAID', 'Not Paid'
    PROCESSING = 'PROCESSING', 'Processing'
    PAID = 'PAID', 'Paid'
    CANCELLED = 'CANCELLED', 'Cancelled'
    FAILED = 'FAILED', 'Failed'


class EnumPaymentMethod(models.TextChoices):
    WALLET = 'WALLET', 'Wallet'
    CASH = 'CASH', 'Cash'
    CARD = 'CARD', 'Card'


class BaseTimestampModel(models.Model):
	created_at = models.DateTimeField(
		verbose_name=u"Created at",
		auto_now_add=True
	)
	updated_at = models.DateTimeField(
		verbose_name=u"Updated at",
		auto_now=True
	)

	class Meta:
		abstract = True


class Transaction(BaseTimestampModel):
    """Represents a single payment operation.

    Transaction is an attempt to pay for delivery fee by a user, 
    with a chosen payment method.
    """
    user = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        null=True
    )
    order = models.ForeignKey(
        Order,
        on_delete=models.SET_NULL,
        null=True
    )
    amount = models.DecimalField(
        max_digits=9,
        decimal_places=2,
        blank=True,
        null=True
    )
    status = models.CharField(
        max_length=15,
        choices=EnumTransactionStatus.choices,
        default=EnumTransactionStatus.NOT_PAID
    )
    payment_method = models.CharField(
        max_length=15,
        choices=EnumPaymentMethod.choices,
        default=EnumPaymentMethod.WALLET
    )

    def __str__(self):
        user_id = user.id if user is not None else "NULL"
        order_id = order.id if order is not None else "NULL"
        
        return f"Order:{order_id} \
                Amount:{self.amount} \
                status:{self.status}"