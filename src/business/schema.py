from django.db.models import Q

import graphene
from graphql import GraphQLError
from graphene import Argument
from graphene_django.types import DjangoObjectType
from graphql_jwt.decorators import login_required
from graphql_jwt.decorators import user_passes_test
from graphql_jwt.decorators import permission_required
from graphql_jwt.decorators import staff_member_required
from graphql_jwt.decorators import superuser_required

from .models import Business
from contacts.models import Address
from contacts.models import Contact
from accounts.models import User
from accounts.models import Biodata
from contacts.schema import AddressInputType
from contacts.schema import ContactInputType
from .models import EnumBusinessCategory as _EnumBusinessCategory

from accounts.schema import ErrorType

EnumBusinessCategory = graphene.Enum.from_enum(_EnumBusinessCategory)


class BusinessType(DjangoObjectType):
    class Meta:
        model = Business
        fields = '__all__'


class Query(object):
    all_businesses = graphene.List(BusinessType, user=graphene.ID())
    business = graphene.Field(BusinessType, id=graphene.ID())

    @staff_member_required
    def resolve_all_businesses(self, info, user=None, **kwargs):
        if user:
            return Business.objects.filter(user=user)
        else:
            # Querying all Businesses
            return Business.objects.all()

    @login_required
    def resolve_business(self, info, id):
        # Querying a single business
        return Business.objects.get(pk=id)


########################BUSINESS##########################
class CreateBusiness(graphene.Mutation):
    business = graphene.Field(BusinessType)
    ok = graphene.Boolean()
    error = graphene.Field(ErrorType)

    class Arguments:
        # The input arguments for this mutation
        user = graphene.ID()
        name = graphene.String()
        contact = graphene.Argument(ContactInputType)
        phone = graphene.String()
        address = graphene.Argument(AddressInputType)
        email = graphene.String()
        category = graphene.Argument(
            EnumBusinessCategory
        )

    @login_required
    def mutate(self, info, name, address, user, category, phone=None, contact=None, email=None):
        user = User.objects.get(pk=user) if user is not None else user
        # Get or create a new address
        if address is not None:
            try:
                address = Address.objects.get(pk=address.id)
            except Address.DoesNotExist:
                address = Address.objects.create(
                    country=address.country,
                    state=address.state,
                    city=address.city,
                    zipcode=address.zipcode,
                    address_line1=address.address_line1,
                    address_line2=address.address_line2
                )
        # Get or create a new contact
        if contact is not None:
            try:
                contact = Contact.objects.get(pk=contact.id)
            except Contact.DoesNotExist:
                contact = Contact.objects.create(
                    name=name,
                    phone=phone,
                    address=address,
                    email=email
                )

        business = Business.objects.create(
            user=user,
            name=name,
            contact=contact,
            phone=phone,
            address=address,
            email=email,
            category=category
        )

        return CreateBusiness(
            business=business, ok=True, error=None
        )


class UpdateBusiness(graphene.Mutation):
    class Arguments:
        # The input arguments for this mutation
        id = graphene.ID()
        user = graphene.ID()
        name = graphene.String()
        contact = graphene.ID()
        phone = graphene.String()
        address = graphene.ID()
        email = graphene.String()
        category = graphene.Argument(
            EnumBusinessCategory
        )

    # The class attributes define the response of the mutation
    business = graphene.Field(BusinessType)

    @login_required
    def mutate(self, info, id, user=None, name=None, address=None, phone=None, contact=None, email=None, category=None):
        try:
            user = User.objects.get(pk=user) if user is not None else user
        except User.DoesNotExist:
            raise GraphQLError({
                'message': 'User does not exists',
                'error_code': 'USER_NOT_FOUND'
            })

        try:
            contact = Contact.objects.get(
                pk=contact) if contact is not None else contact
        except Contact.DoesNotExist:
            raise GraphQLError({
                'message': 'Contact does not exists',
                'error_code': 'CONTACT_NOT_FOUND'
            })

        try:
            address = Address.objects.get(
                pk=address) if address is not None else address
        except Address.DoesNotExist:
            raise GraphQLError({
                'message': 'Address does not exists',
                'error_code': 'ADDRESS_NOT_FOUND'
            })

        try:
            business = Business.objects.get(pk=id)
            business.user = user if user is not None else business.user
            business.name = name if name is not None else business.name
            business.contact = contact if name is not None else business.contact
            business.phone = phone if phone is not None else business.phone
            business.address = address if address is not None else business.address
            business.email = email if email is not None else business.email
            business.category = category if category is not None else business.category
            business.save()
        except Business.DoesNotExist:
            raise GraphQLError({
                'message': 'Business does not exists',
                'error_code': 'BUSINESS_NOT_FOUND'
            })

        # Notice we return an instance of this mutation
        return UpdateBusiness(business=business)


class DeleteBusiness(graphene.Mutation):
    class Arguments:
        # The input arguments for this mutation
        id = graphene.ID()

    # The class attributes define the response of the mutation
    business = graphene.Field(BusinessType)
    ok = graphene.Boolean()

    @login_required
    def mutate(self, info, id):
        try:
            business = Business.objects.get(pk=id)

            if business is not None:
                business.delete()
        except Business.DoesNotExist:
            raise GraphQLError({
                'message': 'Business does not exists',
                'error_code': 'BUSINESS_NOT_FOUND'
            })

        # Notice we return an instance of this mutation
        return DeleteBusiness(business=business, ok=True)


class Mutation(graphene.ObjectType):
    create_business = CreateBusiness.Field()
    update_business = UpdateBusiness.Field()
    delete_business = DeleteBusiness.Field()
