from django.conf import settings
from django.db import models
from django.utils import timezone
from django.conf import settings
from django.utils.translation import gettext as _
from django.contrib.auth import get_user_model
from model_utils import Choices

from contacts.models import Address
from contacts.models import Contact

class EnumBusinessCategory(models.TextChoices):
    FOOD = 'FOOD', 'Food'
    ELECTRONICS = 'ELECTRONICS', 'Electronics & Gadgets'
    CLOTHING = 'CLOTHING', 'Fashion & Clothing'
    HEALTH = 'HEALTH', 'Health & Beauty'
    ECOMMERCE = 'ECOMMERCE', 'Online Vendor & Retail'
    OTHERS = 'OTHERS', 'Others'


class TimestampModel(models.Model):
	created_at = models.DateTimeField(
		verbose_name=u"Created at",
		auto_now_add=True
	)
	updated_at = models.DateTimeField(
		verbose_name=u"Updated at",
		auto_now=True
	)

	class Meta:
		abstract = True


class Business(TimestampModel):
    user = models.OneToOneField(settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE)
    name = models.CharField(max_length=50, verbose_name=_(
        "Business name"))
    contact = models.ForeignKey(Contact, on_delete=models.DO_NOTHING,
        related_name="business", blank=True, null=True)
    phone = models.CharField(max_length=25, blank=True, null=True)
    address = models.ForeignKey(Address, verbose_name=_(
        "Business address"), on_delete=models.DO_NOTHING)
    email = models.CharField(max_length=50, blank=True, null=True)
    category = models.CharField(
        max_length=25,
        choices=EnumBusinessCategory.choices,
        null=True,
        blank=True
    )

    class Meta:
        verbose_name_plural = "Businesses"

    def __str__(self):
        return "{}".format(self.name)
