import time

from django.db import models
from django.utils import timezone
from django.conf import settings
from django.contrib.auth.models import User
from django.utils.translation import gettext as _
from django.utils.text import slugify

from business.models import Business
from couriers.models import Courier
from contacts.models import Address, Contact


class BaseTimestampModel(models.Model):
    created_at = models.DateTimeField(
        verbose_name=u"Created at",
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        verbose_name=u"Updated at",
        auto_now=True
    )

    class Meta:
        abstract = True


class Media(BaseTimestampModel):
    description = models.CharField(
        max_length=255,
        blank=True,
        null=True
    )
    document = models.FileField(
        upload_to='orders/'
    )

    class Meta:
        verbose_name_plural = "Media"


class Event(models.Model):
    description = models.CharField(
        _("Event description"),
        max_length=50
    )
    timestamp = models.DateTimeField(
        auto_now_add=True
    )


class Order(BaseTimestampModel):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, 
        blank=True,
        null=True,
        on_delete=models.SET_NULL
    )
    tracking_id = models.SlugField(
        default='',
        editable=False,
        max_length=10
    )
    status = models.CharField(max_length=10)
    business = models.ForeignKey(
        Business,
        on_delete=models.CASCADE, 
        blank=True,
        null=True
    )
    pickup_contact = models.ForeignKey(
        Contact,
        related_name="pickup_contact", 
        on_delete=models.DO_NOTHING,
        blank=True,
        null=True
    )
    pickup_address = models.ForeignKey(
        Address,
        related_name="pickup_address", 
        on_delete=models.DO_NOTHING,
        blank=True,
        null=True
    )
    delivery_contact = models.ForeignKey(
        Contact,
        related_name="delivery_contact", 
        on_delete=models.DO_NOTHING
    )
    delivery_address = models.ForeignKey(
        Address,
        related_name="delivery_contact",
        on_delete=models.DO_NOTHING
    )
    description = models.CharField(
        max_length=50, blank=True, null=True
    )
    assigned_to = models.ForeignKey(
        Courier,
        on_delete=models.DO_NOTHING, 
        blank=True,
        null=True
    )
    quantity = models.IntegerField(
        default=1, blank=True, null=True
    )
    event = models.ManyToManyField(
        Event, verbose_name=_("Event log"), blank=True
    )
    dimension = models.CharField(
        max_length=25, blank=True, null=True
    )
    weight = models.DecimalField(
        max_digits=3, decimal_places=2, blank=True, null=True
    )
    note = models.TextField(
        max_length=250, blank=True, null=True
    )
    charges = models.DecimalField(
        max_digits=9, decimal_places=2, blank=True, null=True
    )

    class Meta:
        ordering = ['status', 'created_at']

    def __str__(self):
        return f"{self.business} - {self.delivery_contact} : {self.status}"	
    
    # TODO: Refactor how slug is generated (use hashlib.blake2b)
    def save(self, *args, **kwargs):
        if not self.pk:
            # get the current timestamp
            timestamp = "{:10d}".format(int(time.time()))
            # First character from business name and delivery contact
            alphabets = "{0:.1}{1:.1}".format(
                self.business.name,
                self.delivery_contact.name
            )
            # Append timestamp and `alphabets` to the slug
            slug = "{0}{1}".format(timestamp, alphabets)
            # Generate slug
            self.tracking_id = slugify(slug)
        super(Order, self).save(*args, **kwargs)


