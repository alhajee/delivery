import json
import graphene

from django.contrib.auth import get_user_model
from django.db.models import Q
from graphql import GraphQLError
from graphene_django import DjangoObjectType
from graphql_jwt.decorators import login_required
from graphql_jwt.decorators import user_passes_test
from graphql_jwt.decorators import permission_required
from graphql_jwt.decorators import staff_member_required
from graphql_jwt.decorators import superuser_required

from .models import User
from .models import Qualification
from .models import NextOfKin
from .models import WorkExperience
from .models import Biodata
from .models import EnumAccountType as _EnumAccountType
from .models import EnumUserRole as _EnumUserRole

from contacts.models import Address
from contacts.models import Contact


EnumAccountType = graphene.Enum.from_enum(_EnumAccountType)
EnumUserRole = graphene.Enum.from_enum(_EnumUserRole)


class ErrorType(graphene.ObjectType):
    message = graphene.String()
    error_code = graphene.String()


class UserType(DjangoObjectType):
    class Meta:
        model = get_user_model()


class QualificationInputType(graphene.InputObjectType):
    id = graphene.ID()
    institution = graphene.String()
    certificate_type = graphene.String()
    discipline = graphene.String()
    date = graphene.types.datetime.Date()


class QualificationType(DjangoObjectType):
    class Meta:
        model = Qualification


class NextOfKinType(DjangoObjectType):
    class Meta:
        model = NextOfKin


class WorkExperienceType(DjangoObjectType):
    class Meta:
        model = WorkExperience


class BiodataType(DjangoObjectType):
    class Meta:
        model = Biodata

# -----------------------UPLOAD----------------------- #


class PictureUpload(graphene.Scalar):
    def serialize(self):
        pass


class Query(graphene.AbstractType):
    all_users = graphene.List(UserType, search=graphene.String())
    user = graphene.Field(UserType, id=graphene.ID())
    me = graphene.Field(UserType)

    all_user_qualifications = graphene.List(QualificationType)
    user_qualification = graphene.Field(QualificationType, id=graphene.ID())

    all_user_next_of_kins = graphene.List(NextOfKinType)
    user_next_of_kin = graphene.Field(NextOfKinType, id=graphene.ID())

    all_user_work_experiences = graphene.List(WorkExperienceType)
    user_work_experiences = graphene.Field(
        WorkExperienceType, id=graphene.ID())

    all_user_biodata = graphene.List(BiodataType, user=graphene.ID())
    user_biodata = graphene.Field(BiodataType, id=graphene.ID())

    @staff_member_required
    def resolve_all_users(self, info, search=None, **kwargs):
        # The value sent with the search parameter will be in the args variable
        if search:
            filter = (
                Q(first_name__icontains=search) |
                Q(middle_name__icontains=search) |
                Q(last_name__icontains=search)
            )
            return get_user_model().objects.filter(filter)
        else:
            return get_user_model().objects.all()

    @staff_member_required
    def resolve_user(self, info, id):
        return get_user_model().objects.get(pk=id)

    @login_required
    def resolve_me(self, info):
        user = info.context.user
        if user.is_anonymous:
            raise Exception('Not logged in!')
        return user

    @staff_member_required
    def resolve_all_user_qualifications(self, info):
        return Qualification.objects.all()

    @login_required
    def resolve_user_qualification(self, info, id):
        return Qualification.objects.get(pk=id)

    @staff_member_required
    def resolve_all_user_next_of_kins(self, info):
        return NextOfKin.objects.all()

    @login_required
    def resolve_user_next_of_kin(self, info, id):
        return NextOfKin.objects.get(pk=id)

    @staff_member_required
    def resolve_all_user_work_experiences(self, info):
        return WorkExperience.objects.all()

    @login_required
    def resolve_user_work_experience(self, info, id):
        return WorkExperience.objects.get(pk=id)

    @staff_member_required
    def resolve_all_user_biodata(self, info, user=None):
        if user:
            return Biodata.objects.filter(user=user)
        else:
            return Biodata.objects.all()

    @login_required
    def resolve_user_biodata(self, info, id):
        return Biodata.objects.get(pk=id)


class CreateUser(graphene.Mutation):
    # Return object
    user = graphene.Field(UserType)
    ok = graphene.Boolean()
    error = graphene.Field(ErrorType)
    # Arguments

    class Arguments:
        username = graphene.String()
        password = graphene.String()
        email = graphene.String()
        first_name = graphene.String()
        middle_name = graphene.String()
        last_name = graphene.String()
        phone = graphene.String()
        date_updated = graphene.types.datetime.DateTime()

        role = graphene.Argument(
            EnumUserRole
        )

        account_type = graphene.Argument(
            EnumAccountType
        )

    @staticmethod
    def mutate(self, info, password, email, first_name, last_name, account_type, middle_name=None, phone=None, role=None):
        # Check if user already exists
        try:
            user = get_user_model().objects.get(email=email)
            print({"message": "EMAIL_EXIST", "error_code": "EMAIL_EXIST"})
            return CreateUser(ok=False, error={
                "message": "an account with this email address already exists",
                "error_code": "EMAIL_EXIST"
                },
                user=None
            )

            # return ErrorType(message="an account with this email address already exists",
            #                   error_code="EMAIL_ALREADY_EXISTS")
            # raise GraphQLError({
            #     "message": "an account with this email address already exists",
            #     "error_code": "EMAIL_ALREADY_EXISTS"
            # })
        except get_user_model().DoesNotExist:
            user = None

            user = get_user_model()(
                email=email,
                first_name=first_name,
                middle_name=middle_name,
                last_name=last_name,
                phone=phone,
                role=role,
                is_staff=False,
                account_type=account_type
            )
            user.set_password(password)
            user.save()

        return CreateUser(user=user, ok=True, error=None)


class CreateStaff(graphene.Mutation):
    # Return object
    user = graphene.Field(UserType)
    # Arguments

    class Arguments:
        username = graphene.String()
        password = graphene.String()
        email = graphene.String()
        first_name = graphene.String()
        middle_name = graphene.String()
        last_name = graphene.String()
        phone = graphene.String()

        is_staff = graphene.Boolean()
        date_updated = graphene.types.datetime.DateTime()

        account_type = graphene.Argument(
            EnumAccountType
        )

        role = graphene.Argument(
            EnumUserRole
        )

    @superuser_required
    def mutate(self, info, password, email, first_name, last_name, account_type, middle_name=None, phone=None, role=None, is_staff=True):
        # Check if user already exists
        try:
            user = get_user_model().objects.get(email=email)
            raise GraphQLError({
                'message': 'User already exists',
                'error_code': 'USER_ALREADY_EXISTS'
            })
        except get_user_model().DoesNotExist:
            user = None

        user = get_user_model()(
            email=email,
            first_name=first_name,
            middle_name=middle_name,
            last_name=last_name,
            phone=phone,
            role=role,
            is_staff=True,
            account_type=account_type
        )
        user.set_password(password)
        user.save()

        return CreateStaff(user=user)


class UpdateUser(graphene.Mutation):
    user = graphene.Field(UserType)

    class Arguments:
        id = graphene.ID()
        username = graphene.String()
        password = graphene.String()
        email = graphene.String()
        first_name = graphene.String()
        middle_name = graphene.String()
        last_name = graphene.String()
        phone = graphene.String()
        role = graphene.Int()

        is_staff = graphene.Boolean()
        date_updated = graphene.types.datetime.DateTime()

        role = graphene.Argument(
            EnumUserRole
        )

        account_type = graphene.Argument(
            EnumAccountType
        )

    @login_required
    def mutate(self, info, id, password=None, email=None, first_name=None, last_name=None, username=None, middle_name=None, phone=None, role=None, is_staff=None, date_updated=None):
        try:
            user = User.objects.get(pk=id)
            user.email = email if email is not None else user.email
            user.first_name = first_name if first_name is not None else user.first_name
            user.middle_name = middle_name if middle_name is not None else user.middle_name
            user.last_name = last_name if last_name is not None else user.last_name
            user.phone = phone if phone is not None else user.phone
            user.role = role if role is not None else user.role
            user.is_staff = is_staff if is_staff is not None else user.is_staff
            user.date_updated = date_updated if date_updated is not None else user.date_updated
            user.account_type = account_type if account_type is not None else user.account_type

            user.set_password((lambda new, old: old if new is None else new)(
                password, user.password))
            user.save()
        except User.DoesNotExist:
            raise GraphQLError({
                'message': 'User does not exists',
                'error_code': 'USER_NOT_FOUND'
            })
        return UpdateUser(user=user)


class DeleteUser(graphene.Mutation):
    # The class attributes define the response of the mutation
    user = graphene.Field(UserType)
    ok = graphene.Boolean()

    class Arguments:
        # The input arguments for this mutation
        id = graphene.ID()

    @superuser_required
    def mutate(self, info, id):
        try:
            user = User.objects.get(pk=id)
            if user is not None:
                user.delete()
        except User.DoesNotExist:
            raise GraphQLError({
                'message': 'User does not exists',
                'error_code': 'USER_NOT_FOUND'
            })

        # Notice we return an instance of this mutation
        return DeleteUser(user=user, ok=True)

#-------------------QUALIFICATION-------------------#


class CreateQualification(graphene.Mutation):
    # Return object
    qualification = graphene.Field(QualificationType)
    # Arguments

    class Arguments:
        institution = graphene.String()
        certificate_type = graphene.String()
        discipline = graphene.String()
        date = graphene.types.datetime.Date()

    def mutate(self, info, institution, certificate_type, discipline, date):
        qualification = Qualification.objects.create(
            institution=institution,
            certificate_type=certificate_type,
            discipline=discipline,
            date=date
        )
        qualification.save()
        return CreateQualification(qualification=qualification)


class UpdateQualification(graphene.Mutation):
    # Return object
    qualification = graphene.Field(QualificationType)
    # Arguments

    @login_required
    def mutate(self, info, id, institution=None, certificate_type=None, discipline=None, date=None):
        try:
            qualification = Qualification.objects.get(pk=id)
            qualification.institution = institution if institution is not None else qualification.institution
            qualification.certificate_type = certificate_type if certificate_type is not None else qualification.certificate_type
            qualification.discipline = discipline if discipline is not None else qualification.discipline
            qualification.date = date if date is not None else qualification.date

            qualification.save()
        except Qualification.DoesNotExist:
            raise GraphQLError({
                'message': 'Qualification does not exists',
                'error_code': 'QUALIFICATION_NOT_FOUND'
            })

        return UpdateQualification(qualification=qualification)


class DeleteQualification(graphene.Mutation):
    # Return object
    qualification = graphene.Field(QualificationType)
    ok = graphene.Boolean()

    @login_required
    def mutate(self, info, id):
        try:
            qualification = Qualification.objects.get(pk=id)
            if qualification is not None:
                qualification.delete()
        except Qualification.DoesNotExist:
            raise GraphQLError({
                'message': 'Qualification does not exists',
                'error_code': 'QUALIFICATION_NOT_FOUND'
            })

        # Notice we return an instance of this mutation
        return DeleteQualification(qualification=qualification, ok=True)

#-------------------NEXT OF KIN-------------------#


class CreateNextOfKin(graphene.Mutation):
    # Return object
    next_of_kin = graphene.Field(NextOfKinType)
    # Arguments

    class Arguments:
        contact = graphene.ID()
        relationship = graphene.String()

    @login_required
    def mutate(self, info, contact, relationship):
        contact = Contact.objects.get(pk=contact)

        next_of_kin = NextOfKin.objects.create(
            contact=contact,
            relationship=relationship
        )
        next_of_kin.save()
        return CreateNextOfKin(next_of_kin=next_of_kin)


class UpdateNextOfKin(graphene.Mutation):
    # Return object
    next_of_kin = graphene.Field(NextOfKinType)
    # Arguments

    class Arguments:
        id = graphene.ID()
        contact = graphene.ID()
        relationship = graphene.String()

    @login_required
    def mutate(self, info, id, contact=None, relationship=None):
        try:
            next_of_kin = NextOfKin.objects.get(pk=id)

            next_of_kin.contact = Contact.objects.get(
                pk=contact) if contact is not None else next_of_kin.contact
            next_of_kin.relationship = relationship if relationship is not None else next_of_kin.relationship

            next_of_kin.save()

        except NextOfKin.DoesNotExist:
            raise GraphQLError({
                'message': 'Next of Kin does not exists',
                'error_code': 'NEXTOFKIN_NOT_FOUND'
            })
        return UpdateNextOfKin(next_of_kin=next_of_kin)


class DeleteNextOfKin(graphene.Mutation):
    # Return object
    next_of_kin = graphene.Field(NextOfKinType)
    ok = graphene.Boolean()
    # Arguments

    class Arguments:
        # The input arguments for this mutation
        id = graphene.ID()

    @login_required
    def mutate(self, info, id):
        try:
            next_of_kin = NextOfKin.objects.get(pk=id)

            if next_of_kin is not None:
                next_of_kin.delete()
        except NextOfKin.DoesNotExist:
            raise GraphQLError({
                'message': 'Next of Kin does not exists',
                'error_code': 'NEXTOFKIN_NOT_FOUND'
            })

        # Notice we return an instance of this mutation
        return DeleteNextOfKin(next_of_kin=next_of_kin, ok=True)


#-------------------WORK EXPERIENCE-------------------#

class CreateWorkExperience(graphene.Mutation):
    # Return object
    work_experience = graphene.Field(WorkExperienceType)
    # Arguments

    class Arguments:
        company = graphene.String()
        address = graphene.ID()
        job_position = graphene.String()
        start_date = graphene.types.datetime.Date()
        end_date = graphene.types.datetime.Date()
        achievements = graphene.String()
        job_description = graphene.String()
        reason_for_leaving = graphene.String()

    @login_required
    def mutate(self, info, company, job_position, start_date, end_date, job_description, address=None, achievements=None, reason_for_leaving=None):
        address = Address.objects.get(
            pk=address) if address is not None else address

        work_experience = WorkExperience.objects.create(
            company=company,
            address=address,
            job_position=job_position,
            start_date=start_date,
            end_date=end_date,
            achievements=achievements,
            job_description=job_description,
            reason_for_leaving=reason_for_leaving
        )
        work_experience.save()
        return CreateWorkExperience(work_experience=work_experience)


class UpdateWorkExperience(graphene.Mutation):
    # Return object
    work_experience = graphene.Field(WorkExperienceType)
    # Arguments

    class Arguments:
        id = graphene.ID()
        company = graphene.String()
        address = graphene.ID()
        job_position = graphene.String()
        start_date = graphene.types.datetime.Date()
        end_date = graphene.types.datetime.Date()
        achievements = graphene.String()
        job_description = graphene.String()
        reason_for_leaving = graphene.String()

    @login_required
    def mutate(self, info, id, company=None, job_position=None, start_date=None, end_date=None, job_description=None, address=None, achievements=None, reason_for_leaving=None):
        try:
            work_experience = WorkExperience.objects.get(pk=id)
            address = Address.objects.get(
                pk=address) if address is not None else address

            work_experience.company = company if company is not None else work_experience.company
            work_experience.address = address if address is not None else work_experience.address
            work_experience.job_description = job_description if job_description is not None else work_experience.job_description
            work_experience.start_date = start_date if start_date is not None else work_experience.start_date
            work_experience.end_date = end_date if end_date is not None else work_experience.end_date
            work_experience.achievements = achievements if achievements is not None else work_experience.achievements
            work_experience.job_description = job_description if job_description is not None else work_experience.job_description
            work_experience.reason_for_leaving = reason_for_leaving if reason_for_leaving is not None else work_experience.reason_for_leaving

            work_experience.save()
        except WorkExperience.DoesNotExist:
            raise GraphQLError({
                'message': 'Work Experience does not exists',
                'error_code': 'WORKEXPERIENCE_NOT_FOUND'
            })
        return UpdateWorkExperience(work_experience=work_experience)


class DeleteWorkExperience(graphene.Mutation):
    # Return object
    work_experience = graphene.Field(WorkExperienceType)
    ok = graphene.Boolean()
    
    # Arguments
    class Arguments:
        id = graphene.ID()
        
    @login_required
    def mutate(self, info, id):
        try:
            work_experience = WorkExperience.objects.get(pk=id)

            if work_experience is not None:
                work_experience.delete()
        except WorkExperience.DoesNotExist:
            raise GraphQLError({
                'message': 'Work Experience does not exists',
                'error_code': 'WORKEXPERIENCE_NOT_FOUND'
            })
        # Notice we return an instance of this mutation
        return DeleteWorkExperience(work_experience=work_experience, ok=True)


#--------------------BIO DATA-------------------#

class CreateBiodata(graphene.Mutation):
    # Return object
    biodata = graphene.Field(BiodataType)
    # Arguments

    class Arguments:
        user = graphene.ID()
        picture = PictureUpload()
        qualification = graphene.List(graphene.ID)
        work_experience = graphene.List(graphene.ID)
        address = graphene.List(graphene.ID)
        date_of_birth = graphene.types.datetime.Date()
        next_of_kin = graphene.ID()
        religion = graphene.String()

    @login_required
    def mutate(self, info, user, date_of_birth, religion, qualification=None, work_experience=None, address=None, next_of_kin=None):
        # Check if User does not exist
        try:
            user = User.objects.get(pk=user) if user is not None else user
        except User.DoesNotExist:
            raise GraphQLError({
                'message': 'User does not exists',
                'error_code': 'USER_NOT_FOUND'
            })

        # Check if NextOfKin does not exists
        try:
            next_of_kin = NextOfKin.objects.get(
                pk=next_of_kin) if next_of_kin is not None else next_of_kin
        except NextOfKin.DoesNotExist:
            raise GraphQLError({
                'message': 'Next Of Kin does not exists',
                'error_code': 'NEXTOFKIN_NOT_FOUND'
            })

        # Check if Biodata already exists
        try:
            biodata = Biodata.objects.get(pk=user)
            raise GraphQLError({
                'message': 'Biodata already exists',
                'error_code': 'BIODATA_ALREADY_EXISTS'
            })
        except Biodata.DoesNotExist:
            biodata = None

        biodata = Biodata.objects.create(
            user=user,
            date_of_birth=date_of_birth,
            next_of_kin=next_of_kin,
            religion=religion
        )

        if qualification is not None:
            qualification_set = []
            for qualification_id in qualification:
                try:
                    qualification_instance = Qualification.objects.get(
                        pk=qualification_id)
                    qualification_set.append(qualification_instance)
                except Qualification.DoesNotExist:
                    raise GraphQLError({
                        'message': 'Qualification does not exists',
                        'error_code': 'QUALIFICATION_NOT_FOUND'
                    })
            biodata.qualification.set(qualification_set)

        if work_experience is not None:
            work_experience_set = []
            for work_experience_id in work_experience:
                try:
                    work_experience_instance = WorkExperience.objects.get(
                        pk=work_experience_id)
                    work_experience_set.append(work_experience_instance)
                except WorkExperience.DoesNotExist:
                    raise GraphQLError({
                        'message': 'Work Experience does not exists',
                        'error_code': 'WORKEXPERIENCE_NOT_FOUND'
                    })
            biodata.work_experience.set(work_experience_set)

        if address is not None:
            address_set = []
            for address_id in address:
                try:
                    address_instance = Address.objects.get(pk=address_id)
                    address_set.append(address_instance)
                except Address.DoesNotExist:
                    raise GraphQLError({
                        'message': 'Address does not exists',
                        'error_code': 'ADDRESS_NOT_FOUND'
                    })

            biodata.address.set(address_set)

        if info.context.FILES and info.context.method == 'POST':
            picture = info.context.FILES['picture']
            biodata.picture = picture

        biodata.save()
        return CreateBiodata(biodata=biodata)


class UpdateBiodata(graphene.Mutation):
    # Return object
    biodata = graphene.Field(BiodataType)
    # Arguments

    class Arguments:
        id = graphene.ID()
        user = graphene.ID()
        picture = PictureUpload()
        qualification = graphene.List(graphene.ID)
        work_experience = graphene.List(graphene.ID)
        address = graphene.List(graphene.ID)
        date_of_birth = graphene.types.datetime.Date()
        next_of_kin = graphene.ID()
        religion = graphene.String()

    @login_required
    def mutate(self, info, id, user=None, date_of_birth=None, religion=None, qualification=None, work_experience=None, address=None, next_of_kin=None):
        try:
            biodata = Biodata.objects.get(pk=id)

            biodata.user = User.objects.get(
                pk=user) if user is not None else biodata.user
            biodata.date_of_birth = date_of_birth if date_of_birth is not None else biodata.date_of_birth
            biodata.religion = religion if religion is not None else biodata.religion
            biodata.next_of_kin = NextOfKin.objects.get(
                pk=next_of_kin) if next_of_kin is not None else biodata.next_of_kin

            if qualification is not None:
                qualification_set = []
                for qualification_id in qualification:
                    try:
                        qualification_instance = Qualification.objects.get(
                            pk=qualification_id)
                        qualification_set.append(qualification_instance)
                    except Qualification.DoesNotExist:
                        raise GraphQLError({
                            'message': 'Qualification does not exists',
                            'error_code': 'QUALIFICATION_NOT_FOUND'
                        })
                biodata.qualification.set(qualification_set)

            if work_experience is not None:
                work_experience_set = []
                for work_experience_id in work_experience:
                    try:
                        work_experience_instance = WorkExperience.objects.get(
                            pk=work_experience_id)
                        work_experience_set.append(work_experience_instance)
                    except WorkExperience.DoesNotExist:
                        raise GraphQLError({
                            'message': 'Work Experience does not exists',
                            'error_code': 'WORKEXPERIENCE_NOT_FOUND'
                        })
                biodata.work_experience.set(work_experience_set)

            if address is not None:
                address_set = []
                for address_id in address:
                    try:
                        address_instance = Address.objects.get(pk=address_id)
                        address_set.append(address_instance)
                    except Address.DoesNotExist:
                        raise GraphQLError({
                            'message': 'Address does not exists',
                            'error_code': 'ADDRESS_NOT_FOUND'
                        })

                biodata.address.set(address_set)

            if info.context.FILES and info.context.method == 'POST':
                picture = info.context.FILES['picture']
                biodata.picture = picture

            biodata.save()
        except Biodata.DoesNotExist:
            raise GraphQLError({
                'message': 'Biodata does not exists',
                'error_code': 'BIODATA_NOT_FOUND'
            })

        return UpdateBiodata(biodata=biodata)


class DeleteBiodata(graphene.Mutation):
    # Return object
    biodata = graphene.Field(BiodataType)
    ok = graphene.Boolean()
    # Arguments

    class Arguments:
        # The input arguments for this mutation
        id = graphene.ID()

    @login_required
    def mutate(self, info, id):
        try:
            biodata = Biodata.objects.get(pk=id)

            if biodata is not None:
                biodata.delete()
        except Biodata.DoesNotExist:
            raise GraphQLError({
                'message': 'Biodata does not exists',
                'error_code': 'BIODATA_NOT_FOUND'
            })
        # Notice we return an instance of this mutation
        return DeleteBiodata(biodata=biodata, ok=True)


class Mutation(graphene.ObjectType):
    create_user = CreateUser.Field()
    create_staff = CreateStaff.Field()
    update_user = UpdateUser.Field()
    delete_user = DeleteUser.Field()

    create_qualification = CreateQualification.Field()
    update_qualification = UpdateQualification.Field()
    delete_qualification = DeleteQualification.Field()

    create_next_of_kin = CreateNextOfKin.Field()
    update_next_of_kin = UpdateNextOfKin.Field()
    delete_next_of_kin = DeleteNextOfKin.Field()

    create_work_experience = CreateWorkExperience.Field()
    update_work_experience = UpdateWorkExperience.Field()
    delete_work_experience = DeleteWorkExperience.Field()

    create_biodata = CreateBiodata.Field()
    update_biodata = UpdateBiodata.Field()
    delete_biodata = DeleteBiodata.Field()
