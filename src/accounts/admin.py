from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import User, Qualification, NextOfKin, WorkExperience, Biodata

admin.site.register(Qualification)
admin.site.register(NextOfKin)
admin.site.register(WorkExperience)
admin.site.register(Biodata)


class UserAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'phone',
                    'email', 'account_type', 'role', 'is_staff', 'created_at')
    list_filter = ('is_staff', 'account_type', 'role')
    search_fields = ['email', 'phone']


admin.site.register(User, UserAdmin)

# @admin.register(User)
# class CustomUserAdmin(UserAdmin):
#     pass
