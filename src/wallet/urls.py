from django.contrib import admin
from django.urls import path
from django.conf.urls.static import static
from django.conf import settings
#from .views import AuthenticationView, ReserveAccountView

urlpatterns = [
  # path('authentication/', AuthenticationView.as_view(), name="authentication"),
  # path('reserve_account/', ReserveAccountView.as_view(), name="reserve_accoount"),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
