from __future__ import unicode_literals

from django.conf import settings
from django.db import models
from django.utils import timezone
from django.conf import settings
from django.contrib.auth.models import (
    AbstractBaseUser,
    BaseUserManager,
    PermissionsMixin
)
from django.utils.translation import gettext as _
from django.db.models.signals import post_save
from django.dispatch import receiver
from model_utils import Choices

from contacts.models import Address, Contact
from .signals import user_created


class EnumAccountType(models.TextChoices):
    PERSONAL = 'PERSONAL', 'Personal'
    BUSINESS = 'BUSINESS', 'Business'
    COURIER = 'COURIER', 'Courier'
    DRIVER = 'DRIVER', 'Driver'


class EnumUserRole(models.TextChoices):
    AGENT = 'AGENT', 'Agent'
    MANAGER = 'MANAGER', 'Business'
    SUPERVISOR = 'SUPERVISOR', 'Supervisor'


class TimestampModel(models.Model):
    created_at = models.DateTimeField(
        verbose_name=u"Created at",
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        verbose_name=u"Updated at",
        auto_now=True
    )

    class Meta:
        abstract = True


class UserManager(BaseUserManager):
    def _create_user(self, email, password, is_staff, is_superuser, username=None, **extra_fields):
        now = timezone.now()
        if not email:
            raise ValueError('users must have an email address')
        email = self.normalize_email(email)
        user = self.model(
            email=email,
            is_staff=is_staff,
            is_superuser=is_superuser,
            last_login=now,
            **extra_fields
        )
        user.set_password(password)
        user.date_joined = now
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        user = self._create_user(email, password, False, False, **extra_fields)
        return user

    def create_superuser(self, email, password, **extra_fields):
        user = self._create_user(email, password, True, True, **extra_fields)
        return user


class User(AbstractBaseUser, PermissionsMixin, TimestampModel):
    """ Custom user class """
    email = models.CharField(
        _("Email address"), max_length=50, unique=True, db_index=True)
    first_name = models.CharField(
        _("First Name"), max_length=30)
    middle_name = models.CharField(
        _("Middle Name"), max_length=30, blank=True, null=True)
    last_name = models.CharField(
        _("Last Name"), max_length=30)
    phone = models.CharField(
        _("Phone Number"), max_length=14, blank=True, null=True)
    role = models.CharField(
        max_length=25,
        choices=EnumUserRole.choices,
        null=True,
        blank=True
    )
    # TODO: Use Django builtin roles
    account_type = models.CharField(
        max_length=25,
        choices=EnumAccountType.choices,
        default=EnumAccountType.PERSONAL
    )

    is_staff = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'middle_name', 'last_name']

    def __str__(self):
        if self.phone:
            return f"{self.get_full_name()} - {self.phone}"
        else:
            return f"{self.get_full_name()}"

    def get_full_name(self):
        if self.middle_name:
            return f"{self.first_name} {self.middle_name} {self.last_name}".strip()
        else:
            return f"{self.first_name} {self.last_name}".strip()


@receiver(post_save, sender=User)
def call_user_created(sender, instance, created, **kwargs):
    """Send a signal whenever a user object is created."""
    if created:
        user_created.send(sender=User, user=instance)


class Qualification(TimestampModel):
    institution = models.CharField(_("Institution"), max_length=50)
    certificate_type = models.CharField(
        _("Qualification Obtained"), max_length=50)
    discipline = models.CharField(
        _("Discipline"), max_length=50, blank=True, null=True)
    date = models.DateField(
        _("Date obtained"), auto_now=False, auto_now_add=False)


class NextOfKin(TimestampModel):
    contact = models.ForeignKey(Contact, verbose_name=_(
        "Next of Kin"), on_delete=models.CASCADE)
    relationship = models.CharField(max_length=25)

    def __str__(self):
        return "{} - {}".format(self.contact.name, self.contact.phone)


class WorkExperience(TimestampModel):
    company = models.CharField(_("Company Name"), max_length=50)
    address = models.ForeignKey(Address, verbose_name=_(
        "Company address"), on_delete=models.CASCADE, blank=True, null=True)
    job_position = models.CharField(_("Position/Title"), max_length=50)
    start_date = models.DateField(
        _("Start Date"), auto_now=False, auto_now_add=False)
    end_date = models.DateField(
        _("End Date"), auto_now=False, auto_now_add=False)
    achievements = models.CharField(
        _("Key Achievements"), max_length=50, blank=True, null=True)
    job_description = models.TextField(_("Job Description"), max_length=50)
    reason_for_leaving = models.CharField(
        _("Reason for Leaving"), max_length=50, blank=True, null=True)

    def __str__(self):
        return "{} - {}".format(self.company, self.job_position)


class Biodata(TimestampModel):
    user = models.OneToOneField(settings.AUTH_USER_MODEL,
                                on_delete=models.CASCADE, unique=True)
    picture = models.FileField(upload_to='profile/', blank=True, null=True)
    qualification = models.ManyToManyField(
        Qualification, verbose_name=_("Highest Qualification"), blank=True)
    work_experience = models.ManyToManyField(
        WorkExperience, verbose_name=_("Relevant work experience"), blank=True)
    address = models.ManyToManyField(
        Address, verbose_name=_("Current Address"), blank=True)
    date_of_birth = models.DateField(
        auto_now=False, auto_now_add=False, blank=True, null=True)
    next_of_kin = models.OneToOneField(NextOfKin, verbose_name=_(
        "Next of Kin"), blank=True, null=True, on_delete=models.CASCADE)
    religion = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        verbose_name_plural = "Biodata"

    def __str__(self):
        return "{}".format(self.user.get_full_name())
