# Core Python libraries
from hashlib import blake2b
import time

# Django libraries
from django.conf import settings
from django.db import models
from django.utils.translation import gettext as _
from django.dispatch import receiver
from django.db import IntegrityError
from django.core.exceptions import ValidationError


from graphql import GraphQLError

# Project libraries
from accounts.models import User
from accounts.signals import user_created
from .Monnify import Monnify


class BaseTimestampModel(models.Model):
    created_at = models.DateTimeField(
        verbose_name=u"Created at",
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        verbose_name=u"Updated at",
        auto_now=True
    )

    class Meta:
        abstract = True


class Wallet(BaseTimestampModel):
    """ Stores user wallet information """
    
    user = models.OneToOneField(settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE, unique=True)
    account_number = models.CharField(_("Account Number"), max_length=10)
    bank_name = models.CharField(_("Bank Name"), max_length=50)
    bank_code = models.CharField(_("Bank Code"), max_length=4)
    currency_code = models.CharField(
        _("Currency Code"), max_length=4, default="NGN")
    reference = models.CharField(_("Reservation Reference"), max_length=50)
    account_type = models.CharField(_("Reserved Account Type"), max_length=50)
    status = models.CharField(max_length=15, null=True, blank=True)
    
    book_balance = models.DecimalField(max_digits=11, decimal_places=2, default=0)
    available_balance = models.DecimalField(max_digits=11, decimal_places=2, default=0)

    class Meta:
        unique_together = (("user", "currency_code"),)

    def __str__(self):
        description = "{fullname} - {account_number}:{status}".format(
            fullname=self.user.get_full_name(),
            account_number=self.account_number,
            status=self.status
        )
        return description


@receiver(user_created, sender=User)
def handle_user_created(sender, **kwargs):
    """
    Event handler that gets griggered whenever 
    a user_created signal is received
    """
    user = kwargs['user']
    response = create_wallet(user=user, currency_code="NGN")

    if response:
        account_number = response.get("accountNumber")
        bank_name = response.get("bankName")
        bank_code = response.get("bankCode")
        currency_code = response.get("currencyCode")
        reference = response.get("reservationReference")
        account_type = response.get("reservedAccountType")
        status = response.get("status")

        try:
            wallet = Wallet.objects.create(
                user=user,
                account_number=account_number,
                bank_name=bank_name,
                bank_code=bank_code,
                currency_code=currency_code,
                reference=reference,
                account_type=account_type,
                status=status
            )
        except Exception as e:
            print(e)
            # raise ValidationError({
            #     "wallet": ValidationError(
            #         "Cannot create wallet",
            #         code='WALLET_ALREADY_EXIST',
            #     )
            # })


def BVN(lo=1, hi=11):
    def _between(x):
            return lo <= x <= hi
    _between.__docstring__='must be between {} and {}'.format(lo,hi)       
    return _between

def create_wallet(user:User, currency_code:'in NGN', account_bvn:BVN=None)->'Monnify Account':
    """Create a wallet whenever a user_created signal is received"""
    # hash the user id
    h = blake2b(digest_size=8)
    epoch = str(time.time())
    h.update(bytes(str(user.id)+epoch, encoding="utf8"))

    account_ref = h.hexdigest()  # use hash as account reference
    account_name = user.get_full_name()
    email = user.email

    monnify = Monnify()

    account = monnify.create_reserved_account(
        account_name=account_name,
        account_bvn=account_bvn,
        email=email,
        account_ref=account_ref,
        currency=currency_code
    )

    return account

