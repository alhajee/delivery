#######################################################
#                   DEVELOPMENT                       #
#######################################################

FROM python:3.6 AS development

# set environment variables
ENV NODE_ENV development \
    PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1
	
# create app directory
RUN mkdir -p /home/app

# set default dir so that next command will execute in /home/app dir
WORKDIR /home/app

# copy requirements
COPY requirements.txt .

# copy app files to container
COPY src/ .

# execute pip install in /home/app dir because of WORKDIR
RUN pip install -r requirements.txt


# expose port
EXPOSE 8000