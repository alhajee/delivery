from django.db import models
from django.utils import timezone
from django.conf import  settings
from django.utils.translation import gettext as _


class TimestampModel(models.Model):
	created_at = models.DateTimeField(
		verbose_name=u"Created at",
		auto_now_add=True
	)
	updated_at = models.DateTimeField(
		verbose_name=u"Updated at",
		auto_now=True
	)

	class Meta:
		abstract = True


class Address(TimestampModel):
	country = models.CharField(_("Country"), max_length=25)
	state = models.CharField(_("State/Province"), max_length=25)
	city = models.CharField(_("City"), max_length=25)
	zipcode = models.CharField(_("ZipCode"), max_length=10, blank=True, null=True)
	address_line1 = models.CharField(_("Address Line 1"), max_length=50)
	address_line2 = models.CharField(_("Address Line 2"), max_length=50, blank=True, null=True)

	class Meta:	
		verbose_name_plural = "Addresses"

	def __str__(self):
			return f"{self.address_line1} - {self.state}"


class Contact(TimestampModel):
	name = models.CharField(max_length=50)
	phone = models.CharField(max_length=25)
	address = models.ForeignKey(Address, verbose_name=_("Contact Address"), on_delete=models.DO_NOTHING, blank=True, null=True)
	email = models.CharField(max_length=50, blank=True, null=True)

	def __str__(self):
		if self.phone:
			return "{} - {}".format(self.name, self.phone)
		else:
			return "{}".format(self.name)

